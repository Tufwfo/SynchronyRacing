local racingSound = {}

local racingProtocol = require "SynchronyRacing.RacingProtocol"

local event = require "necro.event.Event"
local sound = require "necro.audio.Sound"

function racingSound.play(playerID, name, data)
	racingProtocol.serverSendMessage(playerID, racingProtocol.MessageType.SOUND, {name = name, data = data})
end

event.clientMessage.add("sound", racingProtocol.MessageType.SOUND, function (message)
	sound.playUI(message.name, message.data)
end)

return racingSound
