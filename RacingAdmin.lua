local racingAdmin = {}

local racingConfig = require "SynchronyRacing.RacingConfig"
local racingMatch = require "SynchronyRacing.RacingMatch"
local racingRPC = require "SynchronyRacing.RacingRPC"
local racingReset = require "SynchronyRacing.RacingReset"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local racingTimer = require "SynchronyRacing.RacingTimer"
local racingUsers = require "SynchronyRacing.RacingUsers"
local menu = require "necro.menu.Menu"
local serverPlayerList = require "necro.server.ServerPlayerList"
local serverRooms = require "necro.server.ServerRooms"

local settingsPresets = require "necro.config.SettingsPresets"


local bind = racingRPC.bind
local setter = racingRPC.bindSetter
local cache = racingRPC.bindCache
local defer = racingRPC.bindDeferred

local function resolveMatch(key, match)
	if type(match) == "table" and match.id then
		if key then
			return true, match.id
		else
			match = racingRooms.getMatch(match.id)
			return match ~= nil, match
		end
	end
end

local function resolvePotentiallyLocalMatch(key, match)
	if type(match) == "table" and match.id then
		if key then
			return true, match.id
		else
			local success, resolvedMatch = resolveMatch(match)
			if not success and match then
				resolvedMatch = match
				success = true
			end
			return success, resolvedMatch
		end
	end
end

racingAdmin.Rooms = racingRPC.register("R", {
	create                = bind(racingRooms.create),
	isTeamReady           = cache(racingRooms.isTeamReady),
	isMatchReady          = cache(racingRooms.isMatchReady, resolveMatch),
	forceReady            = bind(racingRooms.forceReady, resolveMatch),
	start                 = bind(racingRooms.start, resolveMatch),
	finish                = bind(racingRooms.finish, resolveMatch),
	close                 = bind(racingRooms.close, resolveMatch),
	pause                 = bind(racingRooms.pause, resolveMatch),
	resume                = bind(racingRooms.resume, resolveMatch),
	listRooms             = cache(racingRooms.listRooms),
	listMatches           = cache(racingRooms.listMatches),
	listCompletedMatches  = cache(racingRooms.listCompletedMatches),
	isValidMatch          = cache(racingRooms.isValidMatch),
	getMatch              = cache(racingRooms.getMatch),
	getMatchByUID         = cache(racingRooms.getMatchByUID),
	getCompletedMatch     = cache(racingRooms.getCompletedMatch),
	getRoom               = cache(racingRooms.getRoom),
	getMatchForRoomID     = cache(racingRooms.getMatchForRoomID),
	getTeamIndexForRoomID = cache(racingRooms.getTeamIndexForRoomID),
	isRacingRoom          = cache(racingRooms.isRacingRoom),
	isParticipant         = cache(racingRooms.isParticipant),
})

racingAdmin.Users = racingRPC.register("U", {
	lookUpUserByPlayerID  = cache(racingUsers.lookUpUserByPlayerID),
	getName               = cache(racingUsers.getName),
	getPlayerID           = cache(racingUsers.getPlayerID),
	isValid               = cache(racingUsers.isValid),
	setSpectatorSlot      = setter(racingUsers.setSpectatorSlot, racingUsers.getSpectatorSlot),
	getSpectatorSlot      = cache(racingUsers.getSpectatorSlot),
	updateLastPlayed      = bind(racingUsers.updateLastPlayed),
	isInMatch             = cache(racingUsers.isInMatch),
	setParticipant        = setter(racingUsers.setParticipant, racingUsers.isParticipant),
	isParticipant         = cache(racingUsers.isParticipant),
	setAdmin              = setter(racingUsers.setAdmin, racingUsers.isAdmin),
	isAdmin               = cache(racingUsers.isAdmin),
	setAvailable          = setter(racingUsers.setAvailable, racingUsers.isAvailable),
	isAvailable           = cache(racingUsers.isAvailable),
	getAvailableUsers     = cache(racingUsers.getAvailableUsers),
	listAllUsers          = cache(racingUsers.listAllUsers),
	addUser               = bind(racingUsers.addUser),
	addConnectedPlayer    = bind(racingUsers.addConnectedPlayer),
})

racingAdmin.Spectator = racingRPC.register("SP", {
	handleAutoSpectate    = bind(racingSpectator.handleAutoSpectate, resolveMatch),
})

racingAdmin.Match = racingRPC.register("M", {
	getTeamLabel          = cache(racingMatch.getTeamLabel),
	getMatchLabel         = cache(racingMatch.getMatchLabel, resolvePotentiallyLocalMatch),
})

racingAdmin.Reset = racingRPC.register("RS", {
	sendRequest           = bind(racingReset.sendRequest),
})

racingAdmin.Config = racingRPC.register("C", {
	getValueForPreset     = cache(racingConfig.getValueForPreset),
	applyRoomSettings     = bind(racingConfig.applyRoomSettings),
	getValueForRoom       = cache(racingConfig.getValueForRoom),
	getValueForMatch      = cache(racingConfig.getValueForMatch, resolveMatch),
	listPresets           = cache(racingConfig.listPresets),
})

racingAdmin.Config.Setting = racingConfig.Setting

racingAdmin.Timer = racingRPC.register("Timer", {
	serverGetRTATime      = cache(racingTimer.serverGetRTATime),
	serverGetRTAAttribute = cache(racingTimer.serverGetRTAAttribute),
})

racingAdmin.Timer.getRTATime = racingTimer.getRTATime

racingAdmin.ServerRooms = racingRPC.register("SR", {
	sendPlayerToDefaultRoom = bind(serverRooms.sendPlayerToDefaultRoom),
	setPlayerRoom           = bind(serverRooms.setPlayerRoom),
	getDefaultRoomID        = cache(serverRooms.getDefaultRoomID),
	isValidRoom             = cache(serverRooms.isValidRoom),
	listRooms               = cache(serverRooms.listRooms),
})

racingAdmin.ServerPlayerList = racingRPC.register("SPL", {
	getRoomID             = cache(serverPlayerList.getRoomID),
	isLoggedIn            = cache(serverPlayerList.isLoggedIn),
	getAttribute          = cache(serverPlayerList.getAttribute),
})

local function closeNamedMenu(name)
	if name == nil or menu.getName() == name then
		menu.close()
	end
end

local function resolveMenuName(name)
	return name or menu.getName()
end

local function clearCacheAndUpdateMenu()
	racingRPC.clearClientCache()
	menu.updateAll()
end

racingAdmin.Menu = racingRPC.register("Menu", {
	close                 = defer(closeNamedMenu, resolveMenuName),
	open                  = defer(menu.open),
	update                = defer(menu.update),
	updateAll             = defer(menu.updateAll),
	clearCache            = defer(clearCacheAndUpdateMenu),
})

return racingAdmin
