local racingProtocol = {}

local serverPlayerList = require "necro.server.ServerPlayerList"
local serverRooms = require "necro.server.ServerRooms"
local serverSocket = require "necro.server.ServerSocket"

local gameClient = require "necro.client.GameClient"
local netplay = require "necro.network.Netplay"
local playerList = require "necro.client.PlayerList"
local room = require "necro.client.Room"

local enum = require "system.utils.Enum"

racingProtocol.MessageType = {
	VICTORY = netplay.MessageType.extend "Victory",
	FORFEIT = netplay.MessageType.extend "Forfeit",
	FLAGPLANT = netplay.MessageType.extend "FP",
	SOUND = netplay.MessageType.extend "Sound",
	SPECTATE = netplay.MessageType.extend "RequestSpectate",
	SPECTATE_CYCLE = netplay.MessageType.extend "SpectateCycle",
	MUSIC_TIME = netplay.MessageType.extend "MT",
	FACTION_SCORES = netplay.MessageType.extend "FS",
	RESTART = netplay.MessageType.extend "Restart",
	RPC = netplay.MessageType.extend "RPC",
	HOLOGRAM = netplay.MessageType.extend "Holo",
	GHOST = netplay.MessageType.extend "G",
}

racingProtocol.PlayerAttribute = {
	TEAM = netplay.PlayerAttribute.extend "TeamID",
	SPECTATOR = netplay.PlayerAttribute.extend "Spectator",
	BROADCASTER = netplay.PlayerAttribute.extend "Broadcaster",
	PARTICIPANT = netplay.PlayerAttribute.extend "Part",
	MATCH = netplay.PlayerAttribute.extend "Match",
	FACTION = netplay.PlayerAttribute.extend "Faction",
	LEVEL = netplay.PlayerAttribute.extend("Level", enum.data {user = true}), -- TODO set level index by host only
	PARENT = netplay.PlayerAttribute.extend "Parent",
	ADMIN = netplay.PlayerAttribute.extend "Admin",
	INITIALIZED = netplay.PlayerAttribute.extend("Init", enum.data {user = true}),
	SPECTATE_CYCLE_LOCKOUT = netplay.PlayerAttribute.extend "SpecLock",
	IDENTITY_PRONOUNS = netplay.PlayerAttribute.extend("IdenP", enum.data {user = true}),
	IDENTITY_NAME = netplay.PlayerAttribute.extend("IdenN", enum.data {user = true}),
	SYSTEM_NAME = netplay.PlayerAttribute.extend("SysNm"),
	GUEST = netplay.PlayerAttribute.extend("Guest"),
}

racingProtocol.RoomAttribute = {
	TIMER = netplay.RoomAttribute.extend "Timer",
	VICTORY_TIMES = netplay.RoomAttribute.extend "VictoryTimes",
	ROOM_TYPE = netplay.RoomAttribute.extend "Type",
	ALL_PLAYERS_PRESENT = netplay.RoomAttribute.extend "All",
	TEAM_COUNT = netplay.RoomAttribute.extend "TeamCount",
	IDENTITY_ENABLED = netplay.RoomAttribute.extend "Identity",
	ENSEMBLE = netplay.RoomAttribute.extend "Ensemble",
	ENSEMBLE_ORDER = netplay.MessageType.extend "EnsembleOrder",
	ENSEMBLE_START = netplay.MessageType.extend "EnsembleStart",
	ENSEMBLE_READY = netplay.MessageType.extend "EnsembleReady",
}

racingProtocol.Resource = {
	FACTIONS = netplay.Resource.extend "Factions",
}

function racingProtocol.clientSendMessage(messageType, messageData)
	return gameClient.sendReliable(messageType, messageData or {}, netplay.Channel.GAME)
end

function racingProtocol.clientSetPlayerAttribute(key, value)
	return playerList.setAttribute(key, value)
end

function racingProtocol.clientGetPlayerAttribute(playerID, key)
	return playerList.getAttribute(playerID, key)
end

function racingProtocol.clientSetRoomAttribute(key, value)
	return room.setAttribute(key, value)
end

function racingProtocol.clientGetRoomAttribute(key)
	return room.getAttribute(key)
end

function racingProtocol.serverSendMessage(playerID, messageType, messageData)
	return serverSocket.sendReliable(messageType, messageData or {}, playerID, netplay.Channel.GAME)
end

function racingProtocol.serverSetPlayerAttribute(playerID, key, value)
	return serverPlayerList.setAttribute(playerID, key, value)
end

function racingProtocol.serverGetPlayerAttribute(playerID, key)
	return serverPlayerList.getAttribute(playerID, key)
end

function racingProtocol.serverSetRoomAttribute(roomID, key, value)
	if serverRooms.isValidRoom(roomID) then
		return serverRooms.setAttribute(roomID, key, value)
	end
end

function racingProtocol.serverGetRoomAttribute(roomID, key)
	if serverRooms.isValidRoom(roomID) then
		return serverRooms.getAttribute(roomID, key)
	end
end

return racingProtocol
