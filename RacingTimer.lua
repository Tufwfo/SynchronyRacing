local racingTimer = {}

local racingConfig = require "SynchronyRacing.RacingConfig"
local racingHologram = require "SynchronyRacing.RacingHologram"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingUtils = require "SynchronyRacing.RacingUtils"

local currentLevel = require "necro.game.level.CurrentLevel"
local ensemble = require "necro.game.data.modifier.Ensemble"
local gameSession = require "necro.client.GameSession"
local hud = require "necro.render.hud.HUD"
local menu = require "necro.menu.Menu"
local multiInstance = require "necro.client.MultiInstance"
local settings = require "necro.config.Settings"
local sound = require "necro.audio.Sound"
local speedrunTimer = require "necro.client.SpeedrunTimer"
local netClock = require "necro.client.NetClock"
local ui = require "necro.render.UI"
local event = require "necro.event.Event"
local serverClock = require "necro.server.ServerClock"
local serverRooms = require "necro.server.ServerRooms"
local color = require "system.utils.Color"

local utils = require "system.utils.Utilities"


delays = {}

showRemainingTime = settings.overridable.bool {
	name = "Show race countdown timer",
	default = false,
	order = 101,
}

local function getRTA(res, clock)
	if type(res) ~= "table" then
		return nil
	elseif res.pause then
		return res.rta
	else
		return (clock or 0) - res.rta
	end
end

function racingTimer.getRTATime(rta)
	rta = rta or racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.TIMER)
	return getRTA(rta, netClock.getTime())
end

function racingTimer.getLevelStartRTATime()
	local res = racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.TIMER)
	return type(res) == "table" and res.start or nil
end

local function updateRTA(roomID, pause, start)
	-- TODO clean up this function
	pause = not not pause
	local res = utils.fastCopy(racingProtocol.serverGetRoomAttribute(roomID, racingProtocol.RoomAttribute.TIMER)) or {}
	local up = start
	if res.pause ~= pause then
		res.rta = serverClock.getTime() - (res.rta or 0)
		res.pause = pause
		up = true
	end
	if start and res.rta then
		res.start = res.pause and res.rta or serverClock.getTime() - res.rta
	end
	if up then
		racingProtocol.serverSetRoomAttribute(roomID, racingProtocol.RoomAttribute.TIMER, res)
	end
end

function racingTimer.serverSetRTATimestamp(roomID, timestamp)
	if serverRooms.isValidRoom(roomID) then
		racingProtocol.serverSetRoomAttribute(roomID, racingProtocol.RoomAttribute.TIMER, {
			start = 0,
			rta = timestamp,
			pause = false,
		})
	end
end

function racingTimer.serverGetRTATime(roomID)
	if serverRooms.isValidRoom(roomID) then
		local res = racingProtocol.serverGetRoomAttribute(roomID, racingProtocol.RoomAttribute.TIMER)
		return getRTA(res, serverClock.getTime())
	end
end

function racingTimer.serverGetRTAAttribute(roomID)
	if serverRooms.isValidRoom(roomID) then
		return racingProtocol.serverGetRoomAttribute(roomID, racingProtocol.RoomAttribute.TIMER)
	end
end

local function drawSpeedrunTimer(seconds, timerName, xoff, yoff, font)
	local time = type(seconds) == "number" and racingUtils.formatTime(seconds) or seconds
	hud.drawText {
		text = timerName and string.format("%s: %s", timerName, time) or time,
		font = font or ui.Font.MEDIUM,
		element = "timer",
		alignY = 1,
		maxWidth = 0,
		maxHeight = 0,
		offsetX = xoff or 0,
		offsetY = yoff or 0,
		useCache = false,
	}
end

lastWarnCount = nil

event.renderGlobalHUD.add("renderRacingSpeedrunTimer", {order = "timer", sequence = -1}, function (ev)
	local mode = gameSession.getCurrentMode()
	if currentLevel.isLobby() or not mode or mode.timerHUD == false or menu.getName() == "runSummary" then
		return
	end

	ev.timerHUD = false

	if multiInstance.isDuplicate() and not racingHologram.getElementVisibility("showSpeedrunTimer") then
		return
	end

	if currentLevel.isSafe() and ensemble.isActive() then
		local rtaTime = racingTimer.getRTATime() or 0
		if not racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ENSEMBLE_START) then
			local countdown = math.max(racingConfig.getClientValue(racingConfig.Setting.ENSEMBLE_PREP_TIME_MAX) - rtaTime, 0)
			local font
			local warnTime = 10
			if countdown < warnTime and not racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ENSEMBLE_READY) then
				local factor = 1 - countdown / warnTime
				local blinkFac = utils.clamp(0, (factor ^ 1.5) * math.sqrt(countdown % 1), 1)
				font = utils.mergeDefaults(ui.Font.MEDIUM, {
					fillColor = color.rgb(255, (1 - blinkFac) * 255, (1 - blinkFac) * 255),
					size = 12 * (1 + 2 * utils.clamp(0, (factor ^ 2) * (countdown % 1), 1)),
				})

				local warnCount = math.ceil(countdown)
				if lastWarnCount ~= warnCount then
					lastWarnCount = warnCount
					if warnCount > 0 and warnCount < 10 then
						sound.playUI("tick")
						if warnCount < 5 then
							sound.playUI("tempoTick" .. (5 - warnCount))
						end
					end
				end
			end
			drawSpeedrunTimer(countdown, nil, 0, 0, font)
		end
		return
	end

	drawSpeedrunTimer(speedrunTimer.getTime(), "Speedrun")
	drawSpeedrunTimer(racingTimer.getRTATime() or speedrunTimer.getTime(), "RTA", 0, -16, ui.Font.SMALL)

	if showRemainingTime then
		local timeLimit = racingConfig.getClientValue(racingConfig.Setting.TIME_LIMIT) or 0
		drawSpeedrunTimer(racingUtils.formatTimeShort(math.max(0, timeLimit - (racingTimer.getRTATime() or 0) )),
			"Timer", 0, -24, ui.Font.SMALL)
	end
end)

event.renderUI.override("renderLowPercentIndicator", 1, function (func, ev) end)
event.renderUI.override("renderSeed", 1, function (func, ev) end)

event.tick.add("racingTimer", {order = "networkServer", sequence = -1}, function (ev)
	local delay = delays[1]
	if delay and serverClock.getTime() >= delay.time then
		delay.func()
		table.remove(delays, 1)
	end
end)

event.serverChangeGameState.add("timer", {order = "broadcast", sequence = 1}, function (ev)
	if not ev.SynchronyRacing_suppressed and ev.message then
		delays[#delays + 1] = {
			func = function ()
				return updateRTA(ev.roomID, ev.message.pause, ev.message.level)
			end,
			time = ev.SynchronyRacing_timestamp or ev.message.time or 0,
		}
	end
end)

return racingTimer
