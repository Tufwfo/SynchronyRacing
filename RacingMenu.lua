local racingMenu = {}

local racingConfig = require "SynchronyRacing.RacingConfig"
local racingHologram = require "SynchronyRacing.RacingHologram"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRPC = require "SynchronyRacing.RacingRPC"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingScore = require "SynchronyRacing.RacingScore"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local racingTimer = require "SynchronyRacing.RacingTimer"
local clientEvents = require "necro.client.ClientEvents"

local event = require "necro.event.Event"
local instantReplay = require "necro.client.replay.InstantReplay"
local menu = require "necro.menu.Menu"
local gameState = require "necro.client.GameState"
local player = require "necro.game.character.Player"
local playerList = require "necro.client.PlayerList"
local replayPlayer = require "necro.client.replay.ReplayPlayer"
local settingsMenu = require "necro.menu.settings.SettingsMenu"
local singlePlayer = require "necro.client.SinglePlayer"

local gameServer = require "necro.server.GameServer"

local utils = require "system.utils.Utilities"

-- TODO fix pause/unpause breaking song timing

consecutiveReplayTicks = 0

local function forfeit()
	menu.open("confirm", {
		message = "Are you sure you want to resign?\nThis will forfeit the current match.",
		yes = function ()
			racingScore.clientSendForfeit()
			instantReplay.stop()
			menu.closeAll()
		end,
		defaultNo = true,
	})
end

local menuEntrySubstitutions = {
	spectate = {enableIf = function ()
		return racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ROOM_TYPE) ~= racingRooms.RoomType.RACE
			and not singlePlayer.isActive() and not replayPlayer.isReady()
	end},
	lobby = {enableIf = function ()
		return not gameState.isInLobby() and player.isCharacterSpawned()
		and racingConfig.getClientValue(racingConfig.Setting.ALLOW_FORFEIT)
	end, label = "Resign", action = forfeit},
	localCoop = {enableIf = false, hideIfDisabled = true},
	restart = {enableIf = function ()
		return gameState.isInGame() and (playerList.getAttribute(nil, racingProtocol.PlayerAttribute.ADMIN)
			or playerList.getAttribute(nil, racingProtocol.PlayerAttribute.PARTICIPANT))
	end},
	pause = {enableIf = false, hideIfDisabled = true},
	leaderboards = {enableIf = false, hideIfDisabled = true},
	replays = {enableIf = false, hideIfDisabled = true},
}

local menuEntryAdditions = {
	continue = {{
		id = "hologram",
		label = "Opponent view",
		action = function ()
			racingHologram.openConfigMenu()
		end,
		hideIfDisabled = true,
		enableIf = function ()
			return racingHologram.isEnabled() or racingSpectator.isClientBroadcaster()
		end,
	}},
	customize = {{
		label = "Edit profile",
		id = "racingIdentity",
		action = function ()
			settingsMenu.open {
				prefix = "mod.SynchronyRacing.identity",
				title = "Edit profile",
			}
		end,
	}, {
		enableIf = function ()
			return gameServer.isOpen() or playerList.getAttribute(nil, racingProtocol.PlayerAttribute.ADMIN)
		end,
		label = "Racing admin",
		id = "admin",
		action = function ()
			racingRPC.clientClearCache()
			menu.open("synchronyRacingAdmin")
		end,
		hideIfDisabled = true,
	}},
}

local function shallowCopyIfNecessary(ev)
	if not ev.SynchronyRacing_shallowCopied then
		ev.menu = utils.shallowCopy(ev.menu)
		ev.menu.entries = ev.menu.entries or {}
		ev.SynchronyRacing_shallowCopied = true
	end
end

local function substituteMenuEntries(ev, subs, adds)
	if replayPlayer.isActive() or not ev.menu or not ev.menu.entries or ev.menu.SynchronyRacing_processed then
		return
	end
	shallowCopyIfNecessary(ev)
	local entries = {}
	for _, entry in ipairs(ev.menu.entries) do
		entries[#entries + 1] = utils.mergeDefaults(utils.shallowCopy(entry), utils.shallowCopy(subs[entry.id] or {}))
		if adds[entry.id] then
			for _, addedEntry in ipairs(adds[entry.id]) do
				entries[#entries + 1] = utils.shallowCopy(addedEntry)
			end
		end
	end
	ev.menu.entries = entries
	ev.menu.SynchronyRacing_processed = true
end

function racingMenu.replaceEntry(ev, entry, ...)
	if type(ev.menu) ~= "table" then
		return
	end

	shallowCopyIfNecessary(ev)

	local extraEntries = {...}
	local entries = {}

	local condition = entry.replaceIf or function (targetEntry, _)
		return targetEntry.id == entry.id
	end
	entry.replaceIf = nil

	for index, targetEntry in ipairs(ev.menu.entries) do
		if type(targetEntry) == "table" and condition(targetEntry, index) then
			entries[#entries + 1] = utils.mergeTables(utils.shallowCopy(targetEntry), entry)
			for _, extraEntry in ipairs(extraEntries) do
				entries[#entries + 1] = utils.shallowCopy(extraEntry)
			end
		else
			entries[#entries + 1] = targetEntry
		end
	end

	ev.menu.entries = entries
end

function racingMenu.removeEntries(ev, id, ...)
	if type(ev.menu) ~= "table" then
		return
	end

	shallowCopyIfNecessary(ev)

	utils.removeIf(ev.menu.entries, type(id) == "function" and id or function (targetEntry)
		return targetEntry.id == id
	end)

	if select("#", ...) > 0 then
		return racingMenu.removeEntries(ev, ...)
	end
end

event.menu.add("pause", {key = "pause", sequence = 1}, function (ev)
	substituteMenuEntries(ev, menuEntrySubstitutions, menuEntryAdditions)
end)

event.menu.add("lobbyMenu", {key = "lobbyMenu", sequence = 1}, function (ev)
	substituteMenuEntries(ev, menuEntrySubstitutions, menuEntryAdditions)
end)

event.menu.add("multiplayerMenu", {key = "networkMultiplayer", sequence = 1}, function (ev)
	substituteMenuEntries(ev, menuEntrySubstitutions, {})
end)

event.menu.add("runSummaryReset", {key = "runSummary", sequence = 1}, function (ev)
	if not ev.menu or not ev.menu.entries then
		return
	end

	racingMenu.removeEntries(ev, "lobby", "replay")
	local summary = ev.arg and ev.arg.summary
	if summary and not summary.victory then
		if ev.arg.final then
			racingMenu.removeEntries(ev, "spectate")
			for _, entry in ipairs(ev.menu.entries) do
				if entry.id == "restart" then
					entry.enableIf = nil
				elseif entry.id == "feedback" then
					entry.enableIf = not not (playerList.getAttribute(nil, racingProtocol.PlayerAttribute.PARTICIPANT)
						and racingConfig.getClientValue(racingConfig.Setting.ALLOW_FORFEIT))
						and (racingTimer.getRTATime() or 0) > racingConfig.getClientValue(racingConfig.Setting.FORFEIT_DELAY)
					entry.label = "Resign"
					entry.action = forfeit
				end
			end
		else
			racingMenu.removeEntries(ev, "feedback")
		end

		ev.menu.escapeAction = function ()
			clientEvents.fireEvent("requestRestart", {
				fromRunSummary = true,
				showConfirmation = false,
			})
		end
	else
		racingMenu.removeEntries(ev, "feedback")
	end
end)

event.gameStateUnpause.add("closePauseMenu", {order = "menu", sequence = 1}, function (ev)
	if not ev.fastForward and not replayPlayer.isActive() then
		menu.closeAll()
	end

	-- Prevent any lingering instant replays
	instantReplay.stop()

	consecutiveReplayTicks = 0
end)

event.tick.add("disableInstantReplay", "instantReplay", function (ev)
	-- Disable instant replay if no menu is open
	if instantReplay.isActive() then
		if menu.isOpen() then
			consecutiveReplayTicks = 0
		elseif consecutiveReplayTicks > 30 then
			instantReplay.stop()
			consecutiveReplayTicks = 0
		else
			consecutiveReplayTicks = consecutiveReplayTicks + 1
		end
	end
end)

event.tick.override("playCountdownSound", {sequence = 1}, function (func, ev)
	-- Don't play countdown sound for long countdowns
	if (gameState.getCountdown() or 0) <= 5 then
		return func(ev)
	end
end)

event.renderUI.add("pauseForceGameRender", {order = "pause", sequence = 1}, function (ev)
	ev.renderGame = true
end)

event.renderUI.override("lowPercent", {sequence = 1}, function (func, ev)
	-- Skip low% indicator
end)

event.gameStateReset.override("closeAllMenus", 1, function (func, ev)
	if not racingSpectator.isClientSpectator() then
		return func(ev)
	end
end)

return racingMenu
