local racingRooms = {}

local racingConfig = require "SynchronyRacing.RacingConfig"
local racingFactions = require "SynchronyRacing.RacingFactions"
local racingMatch = require "SynchronyRacing.RacingMatch"
local racingMatchmaking = require "SynchronyRacing.RacingMatchmaking"
local racingMusicTime = require "SynchronyRacing.RacingMusicTime"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingQueue = require "SynchronyRacing.RacingQueue"
local racingRPC = require "SynchronyRacing.RacingRPC"
local racingSound = require "SynchronyRacing.RacingSound"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local racingUsers = require "SynchronyRacing.RacingUsers"

local serverClock = require "necro.server.ServerClock"
local serverEvents = require "necro.server.ServerEvents"
local serverPlayerList = require "necro.server.ServerPlayerList"
local serverReplay = require "necro.server.ServerReplay"
local serverResources = require "necro.server.ServerResources"
local serverRooms = require "necro.server.ServerRooms"
local serverSocket = require "necro.server.ServerSocket"

local event = require "necro.event.Event"
local menu = require "necro.menu.Menu"
local netplay = require "necro.network.Netplay"
local playerList = require "necro.client.PlayerList"
local settings = require "necro.config.Settings"
local settingsStorage = require "necro.config.SettingsStorage"

local enum = require "system.utils.Enum"
local orderedSelector = require "system.events.OrderedSelector"
local timer = require "system.utils.Timer"
local utils = require "system.utils.Utilities"

local getAttribute = serverPlayerList.getAttribute

local matchTickSelector = orderedSelector.new(event.SynchronyRacing_matchTick, {
	"ensemble",
})

local roomCreateSelector = orderedSelector.new(event.SynchronyRacing_roomCreate, {
	"ensemble",
})

local defaultStartDelay = 10
local defaultPauseDelay = 3
local defaultUnpauseDelay = 3
local defaultAutoPauseDelay = 3
local defaultAutoUnpauseDelay = 3
local defaultFinishDelay = 10

local roomInitPending = true
local menuUpdatePending = false

--- @class SynchronyRacingRoom
--- @field matchID integer Reference to the match this room is associated with
--- @field teamID integer Reference to the team index within the match this room is associated with
--- @field gameState table|nil Pending game state transition

racingRooms.RoomType = enum.sequence {
	LOBBY = 0,
	RACE = 1,
}

nextMatchID = settings.user.number {
	visibility = settings.Visibility.HIDDEN,
	default = 1,
}

--- @type table<integer,SynchronyRacingMatch>
activeMatches = {}

--- @type table<integer,SynchronyRacingRoom>
activeRooms = {}

--- @type table<integer,SynchronyRacingMatch>
completedMatches = settings.user.table {}

function racingRooms.getClientPlayerTeamID(playerID)
	return racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.TEAM)
end

local function updateMenu()
	menuUpdatePending = true
end

--- @param team SynchronyRacingTeam
--- @return integer[] list
--- @return boolean complete
local function getConnectedPlayerList(team)
	local list = {}
	for _, userID in ipairs(team.users) do
		list[#list + 1] = racingUsers.getPlayerID(userID)
	end
	return list, #list == #team.users
end

--- @param match SynchronyRacingMatch
local function initMatchAttributes(match)
	if not match then
		return
	end

	match.mode = racingConfig.getValueForPreset(match.settingsPreset, racingConfig.Setting.TEAM_MODE)
	match = racingFactions.prepareMatch(match)

	match.status = match.status or racingMatch.Status.PENDING
	match.createTimestamp = serverClock.getTime()
	if not match.id then
		local id = nextMatchID
		while activeMatches[id] or completedMatches[id] do
			id = id + 1
		end
		nextMatchID = id
		match.id = id
	end
	match.seed = match.seed or math.random(0, 2^31 - 1)

	for teamID, team in ipairs(match.teams) do
		-- Initialize music time for new race room
		racingMusicTime.setTime(team, 0, 1)
	end

	match.unixCreated = timer.unixTimestamp()
end

local function initPlayerForMatch(playerID, teamID, match)
	-- Look up the faction of this player
	local faction
	local team = match[teamID]
	if team and team.factions then
		faction = team.factions[racingUsers.lookUpUserByPlayerID(playerID)]
	end

	racingProtocol.serverSetPlayerAttribute(playerID, netplay.PlayerAttribute.READY, false)
	racingProtocol.serverSetPlayerAttribute(playerID, netplay.PlayerAttribute.SPECTATING, false)
	racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.TEAM, teamID)
	racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.MATCH, match.id)
	racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.FACTION, faction)
	racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATOR, false)
	racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT, true)

	-- Play a sound to notify players of the race being about to start
	racingSound.play(playerID, "pickupDiamond")
end

--- Creates rooms for a pending match and sends players into those rooms
--- @param match? SynchronyRacingMatch
function racingRooms.create(match)
	match = match or racingQueue.nextMatch() or racingMatchmaking.nextMatch() --[[@as SynchronyRacingMatch]]
	initMatchAttributes(match)
	if match and match.status == racingMatch.Status.PENDING then
		match.status = racingMatch.Status.WAITING
		activeMatches[match.id] = match
		for teamID, team in ipairs(match.teams) do
			if not team.room then
				-- Create room for the racing team
				team.room = serverRooms.createRoom()

				-- Mark as racing room
				serverRooms.setAttribute(team.room, racingProtocol.RoomAttribute.ROOM_TYPE, racingRooms.RoomType.RACE)

				-- Assign team count
				serverRooms.setAttribute(team.room, racingProtocol.RoomAttribute.TEAM_COUNT, #match.teams)

				-- Disallow all settings/mod changes
				serverRooms.setAttribute(team.room, netplay.RoomAttribute.READ_ONLY_SETTINGS, true)

				local players = getConnectedPlayerList(team)
				local ev = {
					match = match,
					matchID = match.id,
					team = team,
					teamID = teamID,
					players = players,
					roomID = team.room,
					host = players[1],
					settings = racingConfig.getAllValuesForPreset(match.settingsPreset),
				}

				roomCreateSelector.fire(ev)

				-- Load game settings and mods for room
				racingConfig.applyRoomSettings(team.room, ev.settings)

				-- Initialize random seed
				serverResources.load(team.room, netplay.Resource.RNG_SEED, nil, match.seed)

				-- Associate room with match
				activeRooms[team.room] = {
					matchID = match.id,
					teamID = teamID,
				}

				-- Update "last played" timestamp
				for _, userID in ipairs(team.users) do
					racingUsers.updateLastPlayed(userID)
				end

				for _, playerID in ipairs(ev.players) do
					serverRooms.setPlayerRoom(playerID, team.room)
					initPlayerForMatch(playerID, teamID, match)
				end

				-- TODO add option to make spectators the host instead
				serverRooms.setHost(team.room, ev.host)
			end
		end

		-- Auto-join spectators
		racingSpectator.handleAutoSpectate(match)
	end
	return match
end

--- Checks if the starting conditions for a given team are fulfilled
--- @param team SynchronyRacingTeam
function racingRooms.isTeamReady(team, ignoreStatus)
	-- The racing room must exist
	if not serverRooms.isValidRoom(team.room) then
		return false
	end

	-- The room must have a valid pending game state
	local roomData = activeRooms[team.room]
	if not roomData or not roomData.gameState then
		return false
	end

	-- The dungeon resource must have been generated already
	if not serverResources.getResource(team.room, netplay.Resource.DUNGEON) then
		return false
	end

	-- All players must be connected to the server
	local players, complete = getConnectedPlayerList(team)
	if not complete then
		return false
	end

	local resourceIDs = getAttribute(players[1], netplay.PlayerAttribute.RESOURCE_VERSIONS)

	-- All players must be logged-in, in the correct room, have all resources downloaded and be ready to start
	for _, playerID in ipairs(players) do
		if not serverPlayerList.isLoggedIn(playerID)
			or serverPlayerList.getRoomID(playerID) ~= team.room
			or (not ignoreStatus and not getAttribute(playerID, netplay.PlayerAttribute.READY))
			or (not ignoreStatus and not getAttribute(playerID, netplay.PlayerAttribute.CHARACTER))
			or not utils.deepEquals(resourceIDs, getAttribute(playerID, netplay.PlayerAttribute.RESOURCE_VERSIONS))
			or getAttribute(playerID, netplay.PlayerAttribute.FAST_FORWARD_ACTIVE)
		then
			return false
		end
	end

	return true
end

--- Checks if the starting conditions for a given match are fulfilled
--- @param match SynchronyRacingMatch
function racingRooms.isMatchReady(match, ignoreStatus)
	if not match or (match.status ~= racingMatch.Status.WAITING and not ignoreStatus) then
		return false
	end

	for teamID, team in ipairs(match.teams) do
		if not racingRooms.isTeamReady(team, ignoreStatus) then
			return false
		end
	end
	return true
end

--- Forces all players in a match to be ready
--- @param match SynchronyRacingMatch
function racingRooms.forceReady(match)
	for _, team in ipairs(match.teams) do
		for _, userID in ipairs(team.users) do
			local playerID = racingUsers.getPlayerID(userID)
			if playerID then
				serverPlayerList.setAttribute(playerID, netplay.PlayerAttribute.READY, true)
			end
		end
	end
	updateMenu()
end

local function getResourceVersionMap(roomID)
	if serverRooms.isValidRoom(roomID) then
		local versions = {}
		for resourceID, resourceData in pairs(serverRooms.getResources(roomID).current) do
			versions[resourceID] = type(resourceData) == "table" and resourceData.version or nil
		end
		return versions
	end
end

local function changeGameState(args)
	return serverEvents.fireEvent("changeGameState", args)
end

--- Starts a pending match if the conditions are fulfilled
--- @param match SynchronyRacingMatch
--- @param delay? number
function racingRooms.start(match, delay)
	if racingRooms.isMatchReady(match) then
		match.status = racingMatch.Status.ACTIVE
		match.unixStart = timer.unixTimestamp()

		-- Emit synchronous game state events
		local startTime = serverClock.getTime() + (delay
			or racingConfig.getValueForMatch(match, racingConfig.Setting.START_COUNTDOWN) or defaultStartDelay)
		match.startTimestamp = startTime
		for teamID, team in ipairs(match.teams) do
			local roomData = activeRooms[team.room]
			local message = utils.fastCopy(roomData.gameState) or {}
			message.resourceVersions = getResourceVersionMap(team.room)
			changeGameState {
				SynchronyRacing_suppressed = false,
				SynchronyRacing_time = startTime,
				roomID = team.room,
				message = message,
			}
		end
		updateMenu()
	end
end

--- Finishes the specified match
--- @param match SynchronyRacingMatch
function racingRooms.finish(match, delay, text)
	if match and match.status ~= racingMatch.Status.FINISHED then
		match.status = racingMatch.Status.FINISHED
		match.closeTimestamp = serverClock.getTime() + (delay or defaultFinishDelay)
		match.unixEnd = timer.unixTimestamp()
		for teamID, team in ipairs(match.teams) do
			changeGameState {
				SynchronyRacing_suppressed = false,
				SynchronyRacing_time = match.closeTimestamp,
				roomID = team.room,
				message = {
					countdown = text or "Match complete! Returning to lobby in",
					state = netplay.GameState.IN_GAME,
					delay = 0,
				},
			}
		end
		updateMenu()
	end
end

--- Finishes the specified match's race rooms
--- @param match SynchronyRacingMatch
function racingRooms.close(match, markIdle)
	if match then
		for teamID, team in ipairs(match.teams) do
			for _, playerID in ipairs(getConnectedPlayerList(team)) do
				if serverPlayerList.getRoomID(playerID) == team.room then
					serverRooms.sendPlayerToDefaultRoom(playerID)
					if markIdle and not serverPlayerList.getAttribute(playerID, netplay.PlayerAttribute.READY) then
						-- Auto-spectate idle players if necessary
						racingSpectator.requestSpectate(playerID, true)
					else
						racingSpectator.requestSpectate(playerID, false)
					end
				end
				-- TODO ensure that this attribute is always cleared for players returning to the main room
				racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.TEAM, nil)
				racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.MATCH, nil)
				racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.FACTION, nil)
				racingProtocol.serverSetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT, nil)
			end
			if serverRooms.isValidRoom(team.room) then
				serverRooms.deleteRoom(team.room)
				activeRooms[team.room] = nil
			end
		end

		-- Log match to completed match table
		completedMatches[match.id] = match
		settingsStorage.saveToFile()

		if activeMatches[match.id] then
			activeMatches[match.id] = nil
			updateMenu()
		end
	end
end

local function pauseUnpause(match, delay, text, paused)
	if match and match.status == racingMatch.Status.ACTIVE then
		-- Emit synchronous game state events
		local pauseTime = serverClock.getTime() + delay
		match.paused = paused
		for teamID, team in ipairs(match.teams) do
			local roomData = activeRooms[team.room]
			roomData.gameState.reset = nil
			changeGameState {
				SynchronyRacing_suppressed = false,
				SynchronyRacing_time = pauseTime,
				roomID = team.room,
				message = {
					countdown = delay > 1 and text or nil,
					state = netplay.GameState.IN_GAME,
					pause = paused,
					delay = 0,
					songTime = racingMusicTime.getTime(team, delay),
				},
			}
		end
		updateMenu()
	end
end

--- Pauses an active match
--- @param match SynchronyRacingMatch
--- @param delay? number
--- @param text? string Text to display during the countdown
function racingRooms.pause(match, delay, text)
	return pauseUnpause(match, delay or defaultPauseDelay, text or "Pausing in", true)
end

--- Resumes an active match
--- @param match SynchronyRacingMatch
--- @param delay? number
--- @param text? string Text to display during the countdown
function racingRooms.resume(match, delay, text)
	return pauseUnpause(match, delay or defaultUnpauseDelay, text or "Unpausing in", false)
end

function racingRooms.listRooms()
	return utils.sort(utils.getKeyList(activeRooms))
end

function racingRooms.listMatches()
	return utils.sort(utils.getKeyList(activeMatches))
end

function racingRooms.listCompletedMatches()
	return utils.sort(utils.getKeyList(completedMatches))
end

--- @return SynchronyRacingMatch
function racingRooms.getMatch(matchID)
	return activeMatches[matchID]
end

--- @return boolean
function racingRooms.isValidMatch(matchID)
	return activeMatches[matchID] ~= nil
end

--- @return SynchronyRacingMatch|false
function racingRooms.getMatchByUID(uid)
	for _, match in pairs(activeMatches) do
		if match.uid == uid then
			return match
		end
	end
	return false
end

--- @return SynchronyRacingMatch
function racingRooms.getCompletedMatch(matchID)
	return completedMatches[matchID]
end

--- @return SynchronyRacingRoom
function racingRooms.getRoom(roomID)
	return activeRooms[roomID]
end

function racingRooms.getMatchForRoomID(roomID)
	return activeMatches[(activeRooms[roomID] or {}).matchID]
end

function racingRooms.getTeamIndexForRoomID(roomID)
	return (activeRooms[roomID] or {}).teamID
end

function racingRooms.isRacingRoom(roomID)
	return serverRooms.isValidRoom(roomID) and not serverReplay.isActive(roomID)
		and serverRooms.getAttribute(roomID, racingProtocol.RoomAttribute.ROOM_TYPE) == racingRooms.RoomType.RACE
end

function racingRooms.isParticipant(playerID)
	local roomData = racingRooms.getRoom(serverPlayerList.getRoomID(playerID)) or {}
	local match = activeMatches[roomData.matchID]
	local userID = racingUsers.lookUpUserByPlayerID(playerID)
	if match and match.teams[roomData.teamID] then
		for _, teamUserID in ipairs(match.teams[roomData.teamID].users) do
			if userID == teamUserID then
				return true
			end
		end
	end
	return false
end

event.serverChangeGameState.add("suppressStateChange", {order = "timestamp", sequence = -2}, function (ev)
	-- By default, disallow all player-initiated state changes
	if ev.playerID and ev.SynchronyRacing_suppressed == nil and racingRooms.isRacingRoom(ev.roomID) then
		ev.SynchronyRacing_suppressed = true

		local roomData = activeRooms[ev.roomID]
		if roomData and ev.message.state == netplay.GameState.IN_GAME then
			local match = activeMatches[roomData.matchID]
			if not roomData.gameState then
				-- Queue up game state change
				if ev.message.reset then
					ev.message.countdown = {text = "Starting in"}
				end
				roomData.gameState = utils.deepCopy(ev.message)
			elseif match and match.status == racingMatch.Status.ACTIVE and ev.message.level then
				-- Ongoing match: allow player-initiated level transitions, but disallow modifying pause state
				ev.SynchronyRacing_suppressed = false
				ev.message.pause = match.paused
			end
		end
	end
end)

event.serverChangeGameState.add("computeEffectiveTimestamp", {order = "timestamp", sequence = 1}, function (ev)
	if ev.SynchronyRacing_time ~= nil then
		ev.message.time = ev.SynchronyRacing_time
	end
end)

event.serverChangeGameState.override("computeTimestamp", {sequence = 1}, function (func, ev)
	if not ev.SynchronyRacing_time then
		return func(ev)
	end
end)

event.serverChangeGameState.override("broadcastStateChange", {sequence = 1}, function (func, ev)
	if not ev.SynchronyRacing_suppressed then
		return func(ev)
	end
end)

local allowedResources = {
	[netplay.Resource.DUNGEON] = true,
	[netplay.Resource.PROGRESSION] = true,
}

event.serverMessage.override("broadcastResource", {sequence = 1}, function (func, ev)
	local roomID = serverPlayerList.getRoomID(ev.playerID)
	local resID = (ev.message or {}).id
	if activeRooms[roomID]
		and not allowedResources[resID]
		and ev.playerID ~= playerList.getLocalPlayerID()
	then
		log.warn("Player %s attempted to send resource %s in race room", serverPlayerList.getName(ev.playerID), resID)
		serverSocket.sendReliable(netplay.MessageType.DISCONNECT, {reconnect = true}, ev.playerID, netplay.Channel.GAME)
	else
		return func(ev)
	end
end)

event.serverLeaveRoom.override("addSpectateAction", {sequence = 1}, function (func, ev)
	if not racingRooms.isRacingRoom(ev.roomID) or not racingConfig.getValueForRoom(ev.roomID, racingConfig.Setting.AUTO_PAUSE) then
		return func(ev)
	end
end)

event.serverLeaveRoom.add("pauseOnDisconnect", {order = "action", sequence = 1}, function (ev)
	local roomData = activeRooms[ev.roomID]
	if roomData and racingRooms.isParticipant(ev.playerID) then
		--- @type SynchronyRacingMatch
		local match = activeMatches[roomData.matchID]
		if match and match.status == racingMatch.Status.ACTIVE and not match.paused
			and racingConfig.getValueForRoom(ev.roomID, racingConfig.Setting.AUTO_PAUSE)
		then
			racingRooms.pause(match, defaultAutoPauseDelay, "Player disconnected, pausing in")
			match.autoPause = true
		end
	end
end)

event.serverLogIn.add("lateJoinRacingRoom", {order = "room", sequence = -1}, function (ev)
	local userID = racingUsers.lookUpUserByPlayerID(ev.playerID)

	-- Player is already in a match room
	if racingUsers.isInMatch(userID) then
		return
	end

	-- Try to associate user with a match, if they're a late-joiner
	for matchID, match in pairs(activeMatches) do
		-- Check if match is waiting for users to join
		if match.status == racingMatch.Status.WAITING then
			-- Try to find the team this user is associated with
			for teamID, team in ipairs(match.teams) do
				for _, participantUserID in ipairs(team.users) do
					if userID == participantUserID then
						-- Found a matching entry: assign player to room and initialize player attributes
						ev.roomID = team.room
						return initPlayerForMatch(ev.playerID, teamID, match)
					end
				end
			end
		end
	end
end)

event.serverCheckPlayerRetention.add("applyRacingRetention", {order = "room", sequence = 1}, function (ev)
	-- Do not retain parented players
	if serverPlayerList.getAttribute(ev.playerID, racingProtocol.PlayerAttribute.PARENT) then
		ev.retain = false
	elseif racingRooms.isRacingRoom(ev.roomID) then
		ev.retain = true
	end
end)

local function isNonLocalPlayer(playerID)
	return not serverSocket.isLocalPlayer(playerID)
end

event.serverAssignHost.add("overrideLobbyRoomHost", {order = "filterParticipants", sequence = 1}, function (ev)
	-- Don't transfer host status to a non-local player in the lobby
	if ev.roomID == serverRooms.getDefaultRoomID() then
		utils.removeIf(ev.playerIDs, isNonLocalPlayer)
	end
end)

event.serverAssignHost.add("unsetLobbyRoomHost", {order = "override", sequence = 1}, function (ev)
	-- Make main lobby room hostless if needed
	if ev.roomID == serverRooms.getDefaultRoomID() and ev.playerID == nil then
		ev.playerID = false
	end
end)

event.serverTick.add("processRacingRooms", {order = "conductor", sequence = -1}, function (ev)
	-- Designate default room as lobby
	if roomInitPending then
		roomInitPending = false
		local roomID = serverRooms.getDefaultRoomID()
		if serverRooms.isValidRoom(roomID) then
			serverRooms.setAttribute(roomID, racingProtocol.RoomAttribute.ROOM_TYPE, racingRooms.RoomType.LOBBY)
		end
	end

	for matchID, match in pairs(activeMatches) do
		matchTickSelector.fire({
			match = match,
			matchID = matchID,
			teams = match.teams,
		})

		if match.status == racingMatch.Status.WAITING then
			-- Check if room "all present" flag needs to be set
			for _, team in ipairs(match.teams) do
				if serverRooms.isValidRoom(team.room)
					and not racingProtocol.serverGetRoomAttribute(team.room, racingProtocol.RoomAttribute.ALL_PLAYERS_PRESENT)
					and select(2, getConnectedPlayerList(team))
				then
					-- Assign players to factions
					if team.factions then
						local factions = {}
						for _, userID in ipairs(team.users) do
							local playerID = racingUsers.getPlayerID(userID)
							if playerID then
								factions[playerID] = team.factions[userID]
							end
						end
						serverResources.load(team.room, racingProtocol.Resource.FACTIONS, "", factions)
					end

					racingProtocol.serverSetRoomAttribute(team.room, racingProtocol.RoomAttribute.ALL_PLAYERS_PRESENT, true)
				end
			end

			-- Auto-start matches when all players are ready
			if racingConfig.getValueForMatch(match, racingConfig.Setting.AUTO_START) then
				racingRooms.start(match)
			elseif not match.readySignalSent and racingRooms.isMatchReady(match) then
				match.readySignalSent = true
				updateMenu()
			end
		end

		-- Unpause matches when all players have reconnected
		if match.autoPause and match.status == racingMatch.Status.ACTIVE and racingRooms.isMatchReady(match, true) then
			match.autoPause = false
			racingRooms.resume(match, defaultAutoUnpauseDelay)
		end

		-- TODO replace soft-close system
		if match.status ~= racingMatch.Status.FINISHED
			and (match.softCloseTimestamp or math.huge) < serverClock.getTime()
			and racingConfig.getValueForMatch(match, racingConfig.Setting.AUTO_CLOSE)
		then
			racingRooms.finish(match, 5)
		end

		-- Close match rooms once finished
		if match.status == racingMatch.Status.FINISHED
			and (match.closeTimestamp or math.huge) < serverClock.getTime()
		then
			racingRooms.close(match)
		end
	end

	if menuUpdatePending then
		menuUpdatePending = false
		racingRPC.serverInvalidateCaches()
	end
end)

event.serverReset.add("resetRacingRooms", "rooms", function (ev)
	activeRooms = {}
	activeMatches = {}
end)

return racingRooms
