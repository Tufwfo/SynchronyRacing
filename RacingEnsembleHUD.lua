local racingSpectator = require "SynchronyRacing.RacingSpectator"
local animationTimer = require "necro.render.AnimationTimer"
local currentLevel = require "necro.game.level.CurrentLevel"
local effectOverlay = require "necro.render.effect.EffectOverlay"
local ensemble = require "necro.game.data.modifier.Ensemble"
local event = require "necro.event.Event"
local hsvFilter = require "necro.render.filter.HSVFilter"
local hud = require "necro.render.hud.HUD"
local instantReplay = require "necro.client.replay.InstantReplay"
local menu = require "necro.menu.Menu"
local objectRenderer = require "necro.render.level.ObjectRenderer"
local outlineFilter = require "necro.render.filter.OutlineFilter"
local render = require "necro.render.Render"
local spectator = require "necro.game.character.Spectator"

local color = require "system.utils.Color"
local ecs = require "system.game.Entities"
local gfx = require "system.gfx.GFX"
local utils = require "system.utils.Utilities"
local timer = require "system.utils.Timer"

local ensembleHUDVisible = false
previewTimer = nil

local function hoverEase(time, power)
	if time < 0.5 then
		return (time * 2) ^ power
	else
		return 2 - (2 - time * 2) ^ power
	end
end

local function hoverHeight(offset)
	local animationTime = animationTimer.getTime() - offset
	animationTime = math.abs(1 - (animationTime * 0.5) % 2)
	local effectValue = hoverEase(animationTime, 2) - 1
	return effectValue * 4
end

local function easeOut(time)
	return utils.clamp(0, 1 - time, 1) ^ 4
end

event.renderGlobalHUD.add("renderEnsemblePreviewHUD", {order = "beatBars", sequence = -1}, function (ev)
	ensembleHUDVisible = ensemble.isActive() and ensemble.getSessionOrder()[1]
		and (spectator.isActive() or instantReplay.isActive())
	if ensembleHUDVisible then
		previewTimer = previewTimer or timer.new()
		local time = previewTimer.getTime()

		local buffer = render.getBuffer(render.Buffer.UI_BOSS_SPLASH)
		local scale = hud.getScaleFactor()
		local factor = 1 - easeOut(time)
		local x, y = 40 * scale, gfx.getHeight() + ((1 - factor) * 40 - 40) * scale
		local order = ensemble.getSessionOrder()
		local maxShift = (gfx.getWidth() - x - 20 * scale) / (#order + 1)
		local currentIndex = currentLevel.getNumber()
		for i, entry in ipairs(order) do
			local entity = ecs.getEntityPrototype(entry)
			if entity and entity.sprite then
				local dx, dy = 0, 0
				if entity.positionalSprite then
					dx, dy = entity.positionalSprite.offsetX, entity.positionalSprite.offsetY + entity.sprite.height - 24
				end
				if entity.characterWithAttachment and entity.hudPlayerListUseAttachmentSprite then
					local head = ecs.getEntityPrototype(entity.characterWithAttachment.attachmentType)
					if head and head.sprite then
						entity = head
					end
				end
				local isCurrent = currentIndex == i
				local isPast = currentIndex > i
				local visual = objectRenderer.getObjectVisual(entity)
				visual.texture = visual.texture and ("/" .. visual.texture)
				visual.rect[1] = dx * scale + x
				visual.rect[2] = dy * scale + y - utils.truncate(utils.clamp(0, visual.rect[4] - 24, 10) + hoverHeight(i * 0.2) - easeOut(time - i * 0.05) * 40) * scale
				visual.rect[3] = visual.rect[3] * scale
				visual.rect[4] = visual.rect[4] * scale
				visual.color = color.fade(visual.color, isCurrent and 1 or factor * (isPast and 0.3 or 0.7))
				x = x + math.min(maxShift, visual.rect[3])
				visual.z = i - 100
				buffer.draw(visual)
				if isCurrent then
					local blink = math.sin(time * 10) * 0.5 + 0.5
					local origTexture = visual.texture
					visual.texture = hsvFilter.getPath(visual.texture, 0, -1, 1)
					visual.color = color.hsv(0.56, 0.2, 1, utils.lerp(0.0, 0.8, blink * blink * blink))
					buffer.draw(visual)
					visual.texture = origTexture
					local outline = outlineFilter.getEntityVisual(entity, visual)
					outline.color = color.hsv(0.56, utils.lerp(0.1, 0.6, blink), 1)
					buffer.draw(outline)
				end
			end
		end
	elseif previewTimer then
		previewTimer = nil
	end
end)

event.renderGlobalHUD.override("renderBeatIndicators", 1, function (func, ev)
	if not ensembleHUDVisible then
		return func(ev)
	end
end)
