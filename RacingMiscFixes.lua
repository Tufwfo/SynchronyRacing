local racingProtocol = require "SynchronyRacing.RacingProtocol"
local action = require "necro.game.system.Action"
local config = require "necro.config.Config"
local currentLevel = require "necro.game.level.CurrentLevel"
local event = require "necro.event.Event"
local beatmap = require "necro.audio.Beatmap"
local menu = require "necro.menu.Menu"
local multiInstance = require "necro.client.MultiInstance"

local netplay = require "necro.network.Netplay"
local gameClient = require "necro.client.GameClient"
local playerList = require "necro.client.PlayerList"

local timer = require "system.utils.Timer"

local missedBeat = action.System.MISSED_BEAT

queueBeatSync = false
unpauseGracePeriod = nil

event.fastForwardComplete.add("fixBeatmap", "menu", function (ev)
	queueBeatSync = true
end)

event.gameStateUnpause.add("fixBeatmap", {order = "music", sequence = 1}, function (ev)
	if queueBeatSync then
		-- Fix invalid song timing when late-joining a paused session
		-- TODO local coop
		beatmap.getPrimary().syncCurrentBeatToMusic()
		queueBeatSync = false
	end

	-- After unpausing, grant 3 seconds of immunity to early beats
	unpauseGracePeriod = timer.getGlobalTime() + 3
end)

event.clientChangeRoom.add("resetBeatmapSync", {order = "resources", sequence = -1}, function (ev)
	queueBeatSync = false
	unpauseGracePeriod = nil
end)

event.clientDisconnect.add("resetBeatmapSync", {order = "reset", sequence = 1}, function (ev)
	queueBeatSync = false
	unpauseGracePeriod = nil
end)

event.clientAddLocalAction.add("unpauseGracePeriod", "rollbackLimit", function (ev)
	if ev.action == missedBeat and unpauseGracePeriod and timer.getGlobalTime() <= unpauseGracePeriod then
		ev.suppressed = true
	end
end)

event.turn.override("spawnLateJoinPlayers", {sequence = 1}, function (func, ev)
	-- Only late-spawn players in the lobby
	if currentLevel.isLobby() then
		return func(ev)
	end
end)

event.trapTrigger.override("openModeSelector", 1, function (func, ev)
	-- No
end)

event.trapTrigger.override("startRun", 1, function (func, ev)
	-- No
end)

event.trapTrigger.override("trapStartDailyChallenge", 1, function (func, ev)
	-- No
end)


event.clientAddLocalAction.override("sendNetworkMessage", 1, function (func, ev)
	-- Disable p2p broadcasts in lobby
	if not currentLevel.isLobby() then
		return func(ev)
	elseif ev.message and ev.playerID == playerList.getLocalPlayerID() then
		gameClient.sendReliable(netplay.MessageType.PLAYER_INPUT, ev.message, netplay.Channel.GAME)
	end
end)

event.tick.override("updateIdleSpectate", 1, function (func, ev)
	-- No
end)

local function skipMenuCloseForAdmins(func, ev)
	if not gameClient.isLoggedIn()
		or not playerList.getAttribute(nil, racingProtocol.PlayerAttribute.ADMIN)
		or playerList.getAttribute(nil, racingProtocol.PlayerAttribute.PARTICIPANT)
		or menu.getName() == "networkConnecting"
		or menu.getName() == "runSummary"
	then
		return func(ev)
	end
end

--event.gameStateReset.override("closeAllMenus", 1, skipMenuCloseForAdmins)
--event.gameStateLevel.override("closeAllLevelTransitionMenus", 1, skipMenuCloseForAdmins)
event.clientRequestRestart.override("closeAllMenus", 1, skipMenuCloseForAdmins)
event.gameStateEnterLobby.override("closeAllMenus", 1, skipMenuCloseForAdmins)
event.fastForwardComplete.override("openPauseMenu", 1, skipMenuCloseForAdmins)
event.gameStateUnpause.override("closePauseMenu", 1, skipMenuCloseForAdmins)

-- TODO TEMP TEST CODE, REMOVE

event.clientDisconnect.add("closeConnection", {order = "closeConnection", sequence = -1}, function (ev)
	if config.osMac then
		log.info("Disconnected %s instance: %s", multiInstance.isDuplicate() and "secondary" or "main", ev.type)
	end
end)
