local racingMusicTime = {}

local racingMatch = require "SynchronyRacing.RacingMatch"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"

local gameServer = require "necro.server.GameServer"
local serverClock = require "necro.server.ServerClock"
local serverPlayerList = require "necro.server.ServerPlayerList"
local serverActionBuffer = require "necro.server.ServerActionBuffer"

local clientActionBuffer = require "necro.client.ClientActionBuffer"
local event = require "necro.event.Event"
local fastForward = require "necro.client.FastForward"
local gameClient = require "necro.client.GameClient"
local gameState = require "necro.client.GameState"
local music = require "necro.audio.Music"
local playerList = require "necro.client.PlayerList"
local timeScale = require "necro.audio.TimeScale"

local sortedList = require "system.utils.SortedList"
local timer = require "system.utils.Timer"

local musicSyncTimer = timer.new()

pauseQueue = {}

--- @param team SynchronyRacingTeam
--- @param offset number|nil
function racingMusicTime.getTime(team, offset)
	if team.musicPaused then
		return team.musicTime
	else
		return team.musicTime + (serverClock.getTime() - team.musicTimeSync + (offset or 0)) * team.musicTimeScale
	end
end

--- @param team SynchronyRacingTeam
--- @param time number|nil
--- @param scale number|nil
function racingMusicTime.setTime(team, time, scale)
	if type(time) ~= "number" then
		time = racingMusicTime.getTime(team)
	end
	if type(scale) ~= "number" then
		scale = 1
	end
	team.musicTime = time
	team.musicTimeScale = scale
	team.musicTimeSync = serverClock.getTime()
end

function racingMusicTime.setPause(team, paused, time, scale)
	racingMusicTime.setTime(team, time, scale)
	team.musicPaused = paused
end

event.serverMessage.add("musicTimeSync", racingProtocol.MessageType.MUSIC_TIME, function (ev)
	-- Only the host can sync music time
	if serverPlayerList.isHost(ev.playerID) and type(ev.message) == "table" then
		local roomID = serverPlayerList.getRoomID(ev.playerID)

		-- Verify that current action buffer ID matches with the message
		local actBufSuccess, actBuf = pcall(serverActionBuffer.getActionBuffer, roomID)
		if not actBufSuccess or ev.message[1] ~= actBuf.getBufferID() then
			return
		end

		local room = racingRooms.getRoom(roomID)
		if not room then
			return
		end

		local match = racingRooms.getMatch(room.matchID)
		if not match or match.status ~= racingMatch.Status.ACTIVE then
			return
		end

		local team = match.teams[room.teamID]
		if not team then
			return
		end

		racingMusicTime.setTime(team, ev.message[2], ev.message[3])
	end
end)

local function syncTime()
	musicSyncTimer.reset()
	local time, scale = timeScale.getTimeAndScale(music.getRawTime())
	-- Correct for latency
	local message = {clientActionBuffer.getCurrentBufferID(), time + (gameClient.getLatency() or 0) * 0.5 * scale}
	-- Only sync time scale if it differs from 1
	if math.abs(scale - 1) > 0.0001 then
		message[3] = scale
	end
	racingProtocol.clientSendMessage(racingProtocol.MessageType.MUSIC_TIME, message)
end

event.tick.add("syncMusicTime", {order = "music", sequence = 1}, function ()
	-- Client: sync music every second
	if gameState.isPlaying() and playerList.isHost() and not fastForward.isActive() and musicSyncTimer.getTime() > 1 then
		syncTime()
	end

	-- Server: sync
	if gameServer.isOpen() then
		while #pauseQueue ~= 0 and serverClock.getTime() >= pauseQueue[1].time do
			local entry = table.remove(pauseQueue, 1)
			racingMusicTime.setPause(entry.team, entry.pause, entry.songTime)
		end
	end
end)

event.gameStatePause.add("syncMusicTime", {order = "music", sequence = -1}, function (ev)
	-- Send client's known music time just before the pause is effective
	if playerList.isHost() and not fastForward.isActive() and musicSyncTimer.getTime() > 1 then
		syncTime()
	end
end)

local function compareTime(entry1, entry2)
	return entry1.time < entry2.time
end

event.serverChangeGameState.add("queuePauseUnpause", {order = "broadcast", sequence = 1}, function (ev)
	if ev.SynchronyRacing_suppressed then
		return
	end

	-- TODO func to get team from roomID
	local room = racingRooms.getRoom(ev.roomID)
	if not room then
		return
	end

	local match = racingRooms.getMatch(room.matchID)
	if not match or match.status ~= racingMatch.Status.ACTIVE then
		return
	end

	local team = match.teams[room.teamID]
	if not team then
		return
	end

	sortedList.insertOrdered(pauseQueue, {
		time = ev.message.time or serverClock.getTime(),
		team = team,
		pause = ev.message.pause,
		songTime = ev.message.level and 0 or nil,
	}, compareTime)
end)

return racingMusicTime
