local racingConfig = require "SynchronyRacing.RacingConfig"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingSpectator = require "SynchronyRacing.RacingSpectator"

local event = require "necro.event.Event"
local gameSession = require "necro.client.GameSession"
local netRNG = require "necro.client.NetRNG"
local netplay = require "necro.network.Netplay"
local player = require "necro.game.character.Player"
local playerList = require "necro.client.PlayerList"
local resources = require "necro.client.Resources"

autoStartPending = false

local function getInitialCharactersFromPlayerList()
	local initialCharacters = {}
	for _, playerID in ipairs(playerList.getPlayerList()) do
		-- Check if player is ready and a participant of the match
		if racingRooms.getClientPlayerTeamID(playerID) == racingRooms.getClientPlayerTeamID()
			and not racingSpectator.isClientSpectator(playerID)
		then
			-- TODO enforce race-specific player characters
			-- Use chosen character, or fall back to first available character
			local characterType = playerList.getAttribute(playerID, netplay.PlayerAttribute.CHARACTER)
			if player.isValidCharacterType(characterType) then
				initialCharacters[playerID] = characterType
			else
				initialCharacters[playerID] = player.getPlayableCharacterTypes()[1]
			end
		end
	end
	return initialCharacters
end

local function allParticipantsReady()
	for _, playerID in ipairs(playerList.getPlayerList()) do
		if not playerList.isReady(playerID)
			and playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT)
		then
			return false
		end
	end

	return true
end

event.tick.add("processRacingClientStart", {order = "lobby", sequence = 1}, function (ev)
	if autoStartPending
		and resources.isResourceListReady()
		and netRNG.getSeed()
		and racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ALL_PLAYERS_PRESENT)
		and allParticipantsReady()
	then
		if playerList.isHost() and not resources.hasData(netplay.Resource.DUNGEON) then
			log.info("Initializing run for racing")
			local parameters = racingConfig.getClientValue(racingConfig.Setting.GAME_MODE)

			parameters.seed = netRNG.getSeed()
			parameters.preserveSeed = true

			parameters.initialCharacters = getInitialCharactersFromPlayerList()
			parameters.preserveInitialCharacters = true

			gameSession.start(parameters)
		end
		autoStartPending = false
	end
end)

event.clientChangeRoom.add("resetRacingClientStart", {order = "resources", sequence = -1}, function (ev)
	autoStartPending = true
end)

event.clientDisconnect.add("resetRacingClientStart", {order = "reset", sequence = 1}, function (ev)
	autoStartPending = false
end)

