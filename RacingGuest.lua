local racingProtocol = require "SynchronyRacing.RacingProtocol"
local action = require "necro.game.system.Action"
local controls = require "necro.config.Controls"
local currentLevel = require "necro.game.level.CurrentLevel"
local gameClient = require "necro.client.GameClient"
local gameServer = require "necro.server.GameServer"
local menu = require "necro.menu.Menu"
local playerList = require "necro.client.PlayerList"
local settings = require "necro.config.Settings"
local singlePlayer = require "necro.client.SinglePlayer"
local textFormat = require "necro.config.i18n.TextFormat"
local ui = require "necro.render.UI"
local color = require "system.utils.Color"
local gfx = require "system.gfx.GFX"

guestMessage = settings.shared.string {
	name = "Guest message",
	desc = "Forces newly joined players into a waiting screen until an admin approves them by signing them up. If left empty, non-signed-up players can join the lobby normally.",
	default = "",
	order = 61,
}

local function isGuest()
	return currentLevel.isLobby()
		and playerList.getAttribute(nil, racingProtocol.PlayerAttribute.GUEST)
		and guestMessage ~= ""
		and not gameServer.isOpen()
		and not singlePlayer.isActive()
end

event.menu.add("guestMenu", "SynchronyRacing_guestMenu", function (ev)
	local y = math.floor(gfx.getHeight() / 2) - 120
	ev.menu = {
		label = "Tournament in progress!",
		entries = {
			{ selected = true },
			{
				label = function ()
					return guestMessage
				end,
				y = y,
			},
			{
				label = "Wait for an administrator to let you in",
				y = gfx.getHeight() - 200,
				font = ui.Font.SMALL,
				color = color.gray(),
			},
			{
				label = string.format("or press %s to disconnect",
					textFormat.color(controls.getFriendlyMiscKeyBind(controls.Misc.CLOSE), color.WHITE)),
				y = gfx.getHeight() - 180,
				font = ui.Font.SMALL,
				color = color.gray(),
			},
		},
		lockSelection = true,
		escapeAction = function ()
			gameClient.disconnect()
		end,
		width = gfx.getWidth(),
		height = gfx.getHeight(),
		fixedWidth = true,
		fixedHeight = true,
		tickCallback = function ()
			if not isGuest() then
				menu.close()
			end
		end,
	}
end)

local playerSpawnAction = action.System.SPAWN_PLAYER_CHARACTER

local function openGuestMenu()
	if isGuest() and not menu.isOpen() then
		menu.open("SynchronyRacing_guestMenu")
	end
end

event.clientAddLocalAction.add("suppressGuestJoin", "rollbackLimit", function (ev)
	if type(ev.action) == "table" and ev.action[1] == playerSpawnAction and isGuest() then
		ev.suppressed = true
	end
end)

event.tick.add("lockGuest", {order = "lobbyLevel", sequence = 1}, function (ev)
	openGuestMenu()
end)
