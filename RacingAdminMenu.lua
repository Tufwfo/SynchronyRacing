local racingAdmin = require "SynchronyRacing.RacingAdmin"
local racingHologram = require "SynchronyRacing.RacingHologram"
local racingMatch = require "SynchronyRacing.RacingMatch"
local racingOpponentHUD = require "SynchronyRacing.RacingOpponentHUD"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRPC = require "SynchronyRacing.RacingRPC"
local racingReset = require "SynchronyRacing.RacingReset"
local racingUtils = require "SynchronyRacing.RacingUtils"
local config = require "necro.config.Config"
local gameClient = require "necro.client.GameClient"
local gameServer = require "necro.server.GameServer"
local multiInstance = require "necro.client.MultiInstance"
local serverSteamLobby = require "necro.server.ServerSteamLobby"
local textPrompt = require "necro.render.ui.TextPrompt"
local stringUtils = require "system.utils.StringUtilities"
local timer = require "system.utils.Timer"

local racingConfig = racingAdmin.Config
local racingRooms = racingAdmin.Rooms
local racingUsers = racingAdmin.Users
local racingTimer = racingAdmin.Timer
local racingSpectator = racingAdmin.Spectator
local serverPlayerList = racingAdmin.ServerPlayerList
local serverRooms = racingAdmin.ServerRooms

local textFormat = require "necro.config.i18n.TextFormat"
local theme = require "necro.config.Theme"

local color = require "system.utils.Color"
local playerList = require "necro.client.PlayerList"
local event = require "necro.event.Event"
local menu = require "necro.menu.Menu"
local netplay = require "necro.network.Netplay"
local remoteSettings = require "necro.config.RemoteSettings"
local settings = require "necro.config.Settings"
local settingsMenu = require "necro.menu.settings.SettingsMenu"
local settingsPresets = require "necro.config.SettingsPresets"
local ui = require "necro.render.UI"

local utils = require "system.utils.Utilities"

-- For performance reasons:

--- @diagnostic disable redundant-value

local smallLowercaseFont = utils.mergeDefaults(ui.Font.SMALL, {uppercase = false})
local mediumLowercaseFont = utils.mergeDefaults(ui.Font.MEDIUM, {uppercase = false})

lastSelectedPreset = settings.user.string { visibility = settings.Visibility.HIDDEN }
lastSelectedUser = settings.user.string { visibility = settings.Visibility.HIDDEN }

--- @type SynchronyRacingMatch
pendingMatch = nil

--- @type SynchronyRacingMatch
lastCreatedMatch = nil

userMenuIncludeOffline = false

local statusNames = {}
for key, value in pairs(racingMatch.Status) do
	statusNames[value] = key
end

--- @param match SynchronyRacingMatch
local function getMatchStatus(match)
	local status = statusNames[match and match.status] or "Closed"
	if match.paused then
		return status .. " (paused)"
	else
		return status
	end
end

local defaultRoomID = 0

local function formatUnixTime(timestamp)
	if type(timestamp) == "number" then
		local success, date = pcall(timer.dateTime, "*t", tonumber(timestamp))
		if success then
			return string.format("%04d-%02d-%02d %02d:%02d:%02d",
				date.year or 0, date.month or 0, date.day or 0, date.hour or 0, date.min or 0, date.sec or 0)
		end
	end
	return nil
end

settings.shared.action {
	name = "Manage presets",
	id = "managePresetsAction",
	autoRegister = true,
	action = function ()
		--menu.open("SynchronyRacing_AdminPresets")
	end,
	enableIf = function ()
		return gameServer.isOpen() or playerList.getAttribute(nil, racingProtocol.PlayerAttribute.ADMIN)
	end,
	-- TODO implement preset menu and make setting visible again
	visibility = settings.Visibility.HIDDEN,
	order = 0,
}

lastChosenTeamCount = settings.user.number {
	default = 0,
	visibility = settings.Visibility.HIDDEN,
}

event.menu.add("synchronyRacingAdmin", "synchronyRacingAdmin", function (ev)
	local entries = {}

	ev.arg = ev.arg or {}

	local showFinished = ev.arg.finished

	if showFinished then
		ev.arg.matches = racingRooms.listCompletedMatches() or ev.arg.matches
	else
		ev.arg.matches = racingRooms.listMatches() or ev.arg.matches
	end

	local matches = ev.arg.matches

	if not matches then
		table.insert(entries, {label = "Loading...", selected = true, hideCursor = true})
	elseif #matches == 0 then
		table.insert(entries, {label = showFinished and "No finished matches" or "No active matches"})
	else
		table.insert(entries, {label = showFinished and "Finished matches" or "Active matches"})

		ev.arg.labelCache = ev.arg.labelCache or {}
		local labelCache = ev.arg.labelCache

		local pageSize = 10
		local maxPage = math.ceil(#matches / pageSize) - 1
		local page = utils.clamp(0, ev.arg.page or 0, maxPage)

		local y = 50
		for i = #matches - (page * pageSize), math.max(1, #matches - ((page + 1) * pageSize) + 1), -1 do
			local matchID = matches[i]
			local match
			if showFinished then
				match = racingRooms.getCompletedMatch(matchID)
			else
				match = racingRooms.getMatch(matchID)
			end

			local label = match and racingAdmin.Match.getMatchLabel(match)
			if showFinished then
				local creationTime = formatUnixTime(match and match.unixCreated)
				if creationTime then
					table.insert(entries, {
						id = "date_" .. tostring(matchID),
						label = creationTime,
						font = smallLowercaseFont,
						y = y,
						x = 300,
						alignX = 1,
					})
				end
			end

			labelCache[matchID] = label or labelCache[matchID] or "Loading..."
			table.insert(entries, {
				id = "match_" .. tostring(matchID),
				label = labelCache[matchID],
				font = smallLowercaseFont,
				action = function ()
					menu.open("synchronyRacingMatchAdmin", {match = match, hideControls = showFinished})
				end,
				specialAction = not showFinished and function ()
					menu.open("confirm", {
						message = {"Selecting match as spectator target.", "Continue?"},
						yes = function ()
							local playerID = playerList.getLocalPlayerID()
							serverRooms.sendPlayerToDefaultRoom(playerID)
							for _, childID in ipairs(playerList.getCrossRoomPlayerList()) do
								if playerList.getAttribute(childID, racingProtocol.PlayerAttribute.PARENT) == playerID then
									serverRooms.sendPlayerToDefaultRoom(childID)
								end
							end
							racingSpectator.handleAutoSpectate(match)
						end,
					})
				end or nil,
				y = y,
			})

			y = y + 24
		end

		if maxPage > 0 then
			table.insert(entries, { height = 0 })

			table.insert(entries, {
				id = "pageNav",
				label = string.format("Page %s/%s", page + 1, maxPage + 1),
				action = function () end,
				leftAction = function ()
					ev.arg.page = page - 1
					menu.update()
				end,
				rightAction = function ()
					ev.arg.page = page + 1
					menu.update()
				end,
				font = smallLowercaseFont,
			})
		end
	end

	table.insert(entries, {height = 0})

	if not showFinished then
		table.insert(entries, {
			label = "Create new match",
			id = "newMatch",
			action = function ()
				menu.open("synchronyRacingMatchNew")
			end,
		})

		table.insert(entries, {
			label = "Recreate last match",
			id = "recreateMatch",
			action = function ()
				menu.open("synchronyRacingMatchNew", {match = lastCreatedMatch})
			end,
			enableIf = lastCreatedMatch ~= nil,
		})

		table.insert(entries, {
			label = "Show match log",
			id = "matchLog",
			action = function ()
				menu.open("synchronyRacingAdmin", {finished = true})
			end,
		})

		table.insert(entries, {
			label = "Commentator mode",
			id = "commentatorMode",
			action = function ()
				menu.open("synchronyRacingCommentatorMode")
			end,
		})

		table.insert(entries, {
			label = "Manage users",
			id = "userList",
			action = function ()
				menu.open("synchronyRacingUserList")
			end,
		})

		table.insert(entries, {
			label = "Settings",
			id = "settings",
			action = function ()
				local hasPermissions = remoteSettings.hasPermissions()
				settingsMenu.open {
					title = "Racing settings",
					prefix = "mod.SynchronyRacing",
					layer = hasPermissions and settings.Layer.REMOTE_PENDING or settings.Layer.REMOTE_OVERRIDE,
					readOnly = not hasPermissions,
				}
			end,
		})

		local lobbyURL = string.format("steam://joinlobby/247080/%s/%s",
			serverSteamLobby.getLobbySteamID() or gameClient.getSteamLobbyID(),
			playerList.getAttribute(nil, netplay.PlayerAttribute.PLATFORM_ID))
		table.insert(entries, utils.mergeTables(textPrompt.menuEntry {
			id = "lobbyID",
			label = function (text)
				if textPrompt.getActiveMenuPrompt() then
					return string.format("%s\n(Press Ctrl+C to copy)", text)
				else
					return string.format("Lobby URL: %s...", text:sub(1, 30))
				end
			end,
			get = function ()
				return lobbyURL
			end,
			set = function () end,
			autoSelect = true,
		}, {
			font = smallLowercaseFont,
		}))
	end

	table.insert(entries, {height = 0})

	table.insert(entries, {
		id = "done",
		label = "Done",
		action = menu.close,
	})

	ev.menu = {
		label = "Racing Admin",
		entries = entries,
		returnCallback = function ()
			racingRPC.clientClearCache()
			menu.update()
		end,
	}
end)

local function togglePause(match, delay)
	-- Unmark autopause
	match.autoPause = false
	if match.paused then
		racingRooms.resume(match, delay)
	else
		racingRooms.pause(match, delay)
	end
end

local statusColorDisconnected = color.rgb(180, 150, 150)
local statusColorReady = color.rgb(100, 250, 120)
local statusColorConnected = color.rgb(100, 220, 250)
local statusColorLocal = color.rgb(250, 220, 100)

local function getStatus(userID, excludeNonConnected, includeReady)
	local playerID = racingUsers.getPlayerID(userID)
	if not playerID or not playerList.getAttribute(playerID, netplay.PlayerAttribute.LOGGED_IN) then
		if not excludeNonConnected then
			return "Offline", statusColorDisconnected
		end
	elseif playerID == playerList.getLocalPlayerID() then
		return "Local", statusColorLocal
	elseif includeReady and playerList.getAttribute(playerID, netplay.PlayerAttribute.READY) then
		return "Ready", statusColorReady
	else
		return "Online", statusColorConnected
	end
end

local function getStatusSuffix(userID, excludeNonConnected, includeReady)
	local status, col = getStatus(userID, excludeNonConnected, includeReady)
	return status and ui.colorizeText(" (" .. status .. ")", col) or ""
end

local function makeTeam()
	return {
		users = {}
	}
end

local function makeMatch()
	local teams = {}
	for i = 1, lastChosenTeamCount do
		teams[i] = makeTeam()
	end
	return {
		teams = teams,
		settingsPreset = lastSelectedPreset,
		mode = racingMatch.TeamMode.SPLIT_ROOMS,
	}
end

local function addToTeam(team, userID)
	table.insert(team.users, userID)
end

local function isPresentInMatch(match, userID)
	for teamID, team in ipairs(match.teams) do
		for _, id in ipairs(team.users) do
			if id == userID then
				return true
			end
		end
	end
end

local function removeFromMatch(match, userID)
	for teamID, team in ipairs(match.teams) do
		utils.removeIf(team.users, function (id)
			return id == userID
		end)
	end
end

event.menu.add("listSelector", "SynchronyRacing_listSelector", function (ev)
	local args = ev.arg or {}
	local entries = {}

	local singular = args.singularText or "Entry"
	local plural = args.pluralText or "Entries"

	local items = args.items
	if #items == 0 then
		table.insert(entries, {label = string.format("No %s", plural)})
	else
		table.insert(entries, {label = plural})
		table.insert(entries, {height = 0})
		local current = args.get()
		for _, item in ipairs(items) do
			table.insert(entries, {
				id = "entry_" .. tostring(item),
				label = args.format(item),
				font = smallLowercaseFont,
				action = function ()
					args.set(item)
					menu.close()
				end,
				selected = (current == item),
			})
		end
	end

	table.insert(entries, {height = 0})

	table.insert(entries, {
		id = "done",
		label = args.doneText or "Cancel",
		action = menu.close,
	})

	ev.menu = {
		label = args.title or string.format("Select %s", singular),
		entries = entries,
	}
end)

local function listSelector(args)
	return {
		id = args.id,
		label = args.label or function ()
			return string.format("%s: %s", args.labelText or args.singularText, args.format(args.get()))
		end,
		action = function ()
			racingRPC.clientClearCache()
			menu.open("SynchronyRacing_listSelector", args)
		end,
		leftAction = function ()
			if #args.items > 0 then
				local index = utils.arrayFind(args.items, args.get()) or 0
				index = index - 1
				if index < 1 then
					index = #args.items
				end
				args.set(args.items[index])
			end
		end,
		rightAction = function ()
			if #args.items > 0 then
				local index = utils.arrayFind(args.items, args.get()) or 0
				index = index + 1
				if index > #args.items then
					index = 1
				end
				args.set(args.items[index])
			end
		end,
	}
end

event.menu.add("synchronyRacingMatchNew", "synchronyRacingMatchNew", function (ev)
	ev.arg = ev.arg or {}

	pendingMatch = pendingMatch or makeMatch()
	ev.arg.match = ev.arg.match or pendingMatch

	-- Preload user list
	local users = racingUsers.listAllUsers()

	--- @type SynchronyRacingMatch
	local match = ev.arg.match

	local entries = {}

	table.insert(entries, listSelector {
		id = "settingsPreset",
		labelText = "Settings",
		singularText = "Preset",
		pluralText = "Settings presets",
		items = utils.concatArrays({""}, racingConfig.listPresets() or {}),
		set = function (preset)
			match.settingsPreset = preset or ""
			lastSelectedPreset = match.settingsPreset
		end,
		get = function () return match.settingsPreset or "" end,
		format = function (preset)
			return preset ~= "" and (settingsPresets.getDisplayName(preset) or preset) or "(Use lobby settings)"
		end,
	})

	table.insert(entries, {height = 0})

	table.insert(entries, {
		id = "teamCount",
		label = match.teams[1] and ("Teams: %d"):format(#match.teams) or "Teams: none",
		action = function () end,
		leftAction = function ()
			if #match.teams > 0 then
				table.remove(match.teams)
				menu.update()
			end
		end,
		rightAction = function ()
			if #match.teams < 32 then
				table.insert(match.teams, makeTeam())
				menu.update()
			end
		end,
	})

	if not match.teams[1] then
		table.insert(entries, {
			id = "noTeamsInfo",
			label = "Selected players will be placed in individual rooms",
			font = smallLowercaseFont,
		})
		if not users then
			table.insert(entries, {
				id = "usersLoading",
				label = "Loading available players...",
				font = smallLowercaseFont,
			})
		else
			ev.arg.lastKnownParticipants = ev.arg.lastKnownParticipants or {}
			ev.arg.autoFillUsers = {}
			for _, userID in ipairs(users) do
				local participant = racingUsers.isParticipant(userID)

				if participant == nil then
					participant = ev.arg.lastKnownParticipants[userID]
				else
					ev.arg.lastKnownParticipants[userID] = participant
				end

				local name = racingUsers.getName(userID)
				local playerID = racingUsers.getPlayerID(userID)
				if playerID and playerList.getAttribute(playerID, netplay.PlayerAttribute.LOGGED_IN)
					and not racingUsers.isInMatch(userID)
				then
					if participant then
						table.insert(ev.arg.autoFillUsers, userID)
					end

					table.insert(entries, {
						id = "user_" .. tostring(userID),
						label = textFormat.checkbox(participant, 1) .. " "
							.. textFormat.fade(name or "Loading...", participant and 1 or 0.6) .. getStatusSuffix(userID),
						font = smallLowercaseFont,
						action = function ()
							racingUsers.setParticipant(userID, not participant)
							ev.arg.lastKnownParticipants[userID] = not participant
							menu.update()
						end,
					})
				end
			end
		end
		table.insert(entries, {height = 0})
	end

	for teamID, team in ipairs(match.teams) do
		table.insert(entries, {
			id = "team" .. teamID,
			label = ("Team %d"):format(teamID),
		})
		for i, userID in ipairs(team.users) do
			table.insert(entries, {
				id = "user_" .. tostring(userID),
				label = (racingUsers.getName(userID) or "Loading...") .. getStatusSuffix(userID),
				font = smallLowercaseFont,
				action = function ()
					menu.changeSelection(1, true)
					removeFromMatch(match, userID)
					menu.update()
				end,
				leftAction = function ()
					removeFromMatch(match, userID)
					addToTeam(match.teams[utils.clamp(1, teamID - 1, #match.teams)], userID)
					menu.update()
				end,
				rightAction = function ()
					removeFromMatch(match, userID)
					addToTeam(match.teams[utils.clamp(1, teamID + 1, #match.teams)], userID)
					menu.update()
				end,
			})
		end
		table.insert(entries, {
			id = "addUser" .. teamID,
			label = "Add player...",
			font = smallLowercaseFont,
			action = function ()
				menu.open("synchronyRacingUserList", {
					title = "Add player",
					subtitle = "Press TAB to select multiple users",
					action = function (userID)
						if not isPresentInMatch(match, userID) then
							return function (alt)
								removeFromMatch(match, userID)
								addToTeam(team, userID)
								if not alt then
									menu.close()
								end
								menu.update()
							end
						end
					end,
					nonConnectedSuffix = true,
					signupFilter = true,
				})
			end,
		})
		table.insert(entries, {height = 0})
	end

	table.insert(entries, {
		id = "create",
		label = "Create",
		action = function ()
			lastCreatedMatch = utils.fastCopy(match)

			-- Remember team count
			lastChosenTeamCount = #match.teams

			-- Use random UID to fetch the right match after creation
			local newMatch = utils.fastCopy(match)
			newMatch.uid = math.random(0, 2^31 - 1)
			newMatch.creator = playerList.getLocalPlayerID()

			-- Auto-fill players if necessary
			if not newMatch.teams[1] and ev.arg.autoFillUsers then
				for i, userID in ipairs(ev.arg.autoFillUsers) do
					local team = makeTeam()
					addToTeam(team, userID)
					table.insert(newMatch.teams, team)
				end
			end

			racingRooms.create(newMatch)

			menu.close()
			menu.update()
			menu.open("synchronyRacingMatchAdmin", { uid = newMatch.uid })

			-- Clear match data
			pendingMatch = makeMatch()
		end,
		enableIf = function ()
			return match.teams[1] or (ev.arg.autoFillUsers and ev.arg.autoFillUsers[1]) or false
		end,
	})

	table.insert(entries, {
		id = "cancel",
		label = "Cancel",
		action = menu.close,
	})

	ev.menu = {
		label = "Create new match",
		entries = entries,
		directionalConfirmation = false,
	}
end)

--- @param team SynchronyRacingTeam
--- @return string
local function getLabelSuffix(team)
	if team.placement then
		local text = racingUtils.placementToString(team.placement)
		if team.victoryTime then
			text = text .. ": " .. racingUtils.formatTimeMilliseconds(team.victoryTime)
		end
		return ui.colorizeText(" (" .. text .. ")", team.placement == 1 and statusColorReady or statusColorLocal)
	else
		local suffix = ""
		if type(team.flagPlant) == "table" and team.flagPlant.depth and team.flagPlant.floor then
			suffix = ": " .. racingOpponentHUD.formatLevel(team.flagPlant.depth, team.flagPlant.floor) .. " "
				.. (type(team.flagPlant.time) == "number" and racingUtils.formatTimeShort(team.flagPlant.time) or "")
		end
		if team.forfeit then
			return suffix .. ui.colorizeText(" (Forfeit)", statusColorDisconnected)
		else
			return suffix
		end
	end
end

local function getScoreInfix(score)
	if score then
		return string.format(" - %s pts", ui.colorizeText(tostring(score), statusColorLocal))
	else
		return ""
	end
end

event.menu.add("multiSelect", "SynchronyRacing_multiSelect", function (ev)
	local entries = {}
	ev.arg = ev.arg or {}
	local args = ev.arg

	args.checkboxes = args.checkboxes or {}

	local header = args.header
	if type(header) == "string" then
		header = stringUtils.split(header, "\n")
	end

	if type(header) == "table" then
		for _, line in ipairs(header) do
			entries[#entries + 1] = {
				label = line,
			}
		end
		entries[#entries + 1] = { height = 0 }
	end

	local selectables = args.entries or {}
	local allChecked = true
	local noneChecked = (next(args.checkboxes) == nil)

	for i = 1, #selectables do
		if not args.checkboxes[i] then
			allChecked = false
			break
		end
	end

	entries[#entries + 1] = {
		id = "_selectAll",
		label = string.format("%s Select all", textFormat.checkbox(allChecked)),
		alignX = 0,
		x = -200,
		action = function ()
			if allChecked then
				utils.clearTable(args.checkboxes)
			else
				for i = 1, #selectables do
					args.checkboxes[i] = true
				end
			end
			menu.update()
		end,
		selected = true,
	}

	entries[#entries + 1] = { height = 0 }

	for i, entry in ipairs(selectables) do
		entries[#entries + 1] = {
			id = entry.id,
			label = string.format("%s %s", textFormat.checkbox(args.checkboxes[i]), utils.eval(entry.label, "")),
			alignX = 0,
			x = -200,
			action = function ()
				args.checkboxes[i] = not args.checkboxes[i] or nil
				menu.update()
			end,
			font = mediumLowercaseFont,
		}
	end

	entries[#entries + 1] = { height = 0 }

	entries[#entries + 1] = {
		id = "_done",
		label = args.labelDone or "Done",
		action = function ()
			menu.close()
			local result = {}
			for i, entry in ipairs(selectables) do
				if args.checkboxes[i] then
					result[#result + 1] = entry
				end
			end
			args.callback(result)
		end,
		enableIf = not not (args.allowEmpty or not noneChecked),
	}

	entries[#entries + 1] = {
		id = "_cancel",
		label = args.labelCancel or "Cancel",
		action = function ()
			menu.close()
		end,
	}

	ev.menu = {
		label = args.title,
		entries = entries,
	}
end)

--- @param match SynchronyRacingMatch
--- @param header string
--- @param labelDone string
--- @param callback function
local function selectTargetTeams(match, header, labelDone, callback)
	if not match or not match.teams then
		return
	end

	local entries = {}
	for i, team in ipairs(match.teams) do
		entries[#entries + 1] = {
			id = "team_" .. i,
			label = function ()
				return racingAdmin.Match.getTeamLabel(team)
			end,
			index = i,
		}
	end

	menu.open("SynchronyRacing_multiSelect", {
		header = header,
		entries = entries,
		callback = function (selected)
			callback(utils.map(selected, function (entry)
				return match.teams[entry.index]
			end))
		end,
		labelDone = labelDone,
	})
end

event.menu.add("synchronyRacingMatchAdmin", "synchronyRacingMatchAdmin", function (ev)
	local entries = {}

	ev.arg = ev.arg or {}

	if ev.arg.match == nil and ev.arg.uid then
		ev.arg.match = racingRooms.getMatchByUID(ev.arg.uid)
		table.insert(entries, {label = "Loading match..."})
	elseif ev.arg.match and ev.arg.match.id then
		ev.arg.match = racingRooms.getMatch(ev.arg.match.id) or ev.arg.match
	end

	if ev.arg.match == false then
		table.insert(entries, {label = "Invalid match"})
	end

	--- @type SynchronyRacingMatch
	local match = ev.arg.match or {}

	table.insert(entries, {label = racingAdmin.Match.getMatchLabel(match), font = smallLowercaseFont})
	table.insert(entries, {height = 0})

	local function isValid()
		return match.id and racingRooms.isValidMatch(match.id)
	end

	local teams = match.teams or {}
	for i, team in ipairs(teams) do
		table.insert(entries, {
			id = "team_" .. tostring(i),
			label = function ()
				return ("Room %d%s"):format(i, getLabelSuffix(team))
			end,
			font = mediumLowercaseFont,
			action = function () end,
			specialAction = function ()
				serverRooms.setPlayerRoom(playerList.getLocalPlayerID(), team.room)
			end,
			enableIf = function ()
				return serverRooms.isValidRoom(team.room)
			end,
		})
		for _, userID in ipairs(team.users) do
			-- TODO clean up this whole section of code
			local prefix = (racingUsers.getName(userID) or "[...]")
			local factions = team.factions or {}
			if match.mode == racingMatch.TeamMode.SAME_ROOM and factions[userID] then
				prefix = string.format("%s - Team %s", prefix, factions[userID])
			end
			table.insert(entries, {
				id = "user_" .. tostring(userID),
				label = function ()
					return prefix
						.. getScoreInfix(team.factionScores and team.factionScores[factions[userID]])
						.. getStatusSuffix(userID, nil, match.status == racingMatch.Status.WAITING)
				end,
				font = smallLowercaseFont,
				action = function () end,
				selectableIf = false,
			})
		end
		table.insert(entries, {height = 0})
	end

	table.insert(entries, utils.mergeTables(textPrompt.menuEntry {
		id = "seedReadOnly",
		label = function (text)
			if textPrompt.getActiveMenuPrompt() then
				return string.format("Seed: %s (Ctrl+C to copy)", text)
			else
				return string.format("Seed: %s", text)
			end
		end,
		get = function ()
			return match.seed
		end,
		set = function () end,
		autoSelect = true,
	}, {
		font = smallLowercaseFont,
	}))

	if not ev.arg.hideControls then
		table.insert(entries, {
			label = "Status: " .. getMatchStatus(match),
			font = smallLowercaseFont,
		})

		table.insert(entries, {
			label = function ()
				-- TODO use netclock timestamp for this instead
				local room = (teams[1] or {}).room
				local rta = room and racingTimer.serverGetRTAAttribute(room)
				local time = rta and racingTimer.getRTATime(rta)
				local limit = racingConfig.getValueForMatch(match, racingConfig.Setting.TIME_LIMIT)
				if time and limit and limit > 0 then
					time = limit - time
					if time < 0 then
						time = -time
						return ("Timer: -%s"):format(racingUtils.formatTimeShort(time))
					end
				end
				return time and ("Timer: %s"):format(racingUtils.formatTimeShort(time)) or ""
			end,
			alignX = 0,
			x = -40,
			font = smallLowercaseFont,
		})

		table.insert(entries, {height = 0})

		table.insert(entries, {
			font = smallLowercaseFont,
			label = "Force ready",
			id = "forceReady",
			action = function ()
				menu.open("confirm", {
					message = {"Are you sure you want to override", "all players' ready states?"},
					yes = function ()
						racingRooms.forceReady(match)
					end,
				})
			end,
			enableIf = function ()
				return isValid() and match.status == racingMatch.Status.WAITING
			end,
		})

		table.insert(entries, {
			font = smallLowercaseFont,
			label = "Start race",
			id = "start",
			action = function ()
				racingRooms.start(match)
			end,
			enableIf = function ()
				return isValid() and racingRooms.isMatchReady(match)
			end,
		})

		table.insert(entries, {
			font = smallLowercaseFont,
			label = function()
				return match.paused and "Resume race" or "Pause race"
			end,
			id = "pause",
			action = function () togglePause(match) end,
			specialAction = function () togglePause(match, 0.25) end,
			enableIf = function ()
				return isValid() and match.status == racingMatch.Status.ACTIVE
			end,
		})

		local function makeResetEntry(target, id, label, header)
			return {
				font = smallLowercaseFont,
				id = id,
				label = label,
				action = function ()
					selectTargetTeams(match, header, label, function (selection)
						for _, team in ipairs(selection) do
							racingAdmin.Reset.sendRequest(team.room, target)
						end
					end)
				end,
				enableIf = function ()
					return isValid() and match.status == racingMatch.Status.ACTIVE
				end,
			}
		end

		table.insert(entries, makeResetEntry(racingReset.Target.FIRST_LEVEL, "resetRun", "Reset run",
			"Select players to be reset\nback to 1-1, same seed:"))

		table.insert(entries, makeResetEntry(racingReset.Target.CURRENT_LEVEL, "resetFloor", "Restart current floor",
			"Select players to reset to the same floor.\nWarning! Will cause seed variance."))

		table.insert(entries, makeResetEntry(racingReset.Target.NEXT_LEVEL, "resetNextFloor", "Force next level",
			"Select players to force a\nlevel transition for:"))

		table.insert(entries, {
			font = smallLowercaseFont,
			label = "Finish race",
			id = "finish",
			action = function ()
				menu.open("confirm", {
					message = "Are you sure you want to finish this race?",
					yes = function ()
						if match.paused then
							racingRooms.resume(match, 0)
						end
						racingRooms.finish(match)
						menu.updateAll()
					end,
				})
			end,
			specialAction = function ()
				menu.open("confirm", {
					message = "Confirm race finish? (short countdown)",
					yes = function ()
						if match.paused then
							racingRooms.resume(match, 0)
						end
						racingRooms.finish(match, 3)
						menu.updateAll()
					end,
				})
			end,
			enableIf = function ()
				return isValid() and match.status == racingMatch.Status.ACTIVE
			end,
		})

		table.insert(entries, {
			font = smallLowercaseFont,
			label = "Close race room",
			id = "close",
			action = function ()
				menu.open("confirm", {
					message = "Are you sure you want to close this race room?",
					yes = function ()
						if match.status == racingMatch.Status.ACTIVE then
							racingRooms.finish(match, 3, "Match closing in")
						else
							racingRooms.close(match)
						end
						menu.updateAll()
					end,
				})
			end,
			specialAction = function ()
				if config.dev then
					racingRooms.close(match)
				end
			end,
			enableIf = function ()
				return isValid()
			end,
		})
	end

	table.insert(entries, {height = 0})

	table.insert(entries, {
		id = "done",
		label = "Done",
		action = menu.close,
	})

	ev.menu = {
		label = "Match settings",
		entries = entries,
	}
end)

event.menu.add("synchronyRacingUserList", "synchronyRacingUserList", function (ev)
	local arg = ev.arg or {}
	ev.arg = arg
	local entries = {}

	arg.action = arg.action or function (userID)
		return function (_)
			menu.open("synchronyRacingUserAdmin", {user = userID})
		end
	end

	local users = racingUsers.listAllUsers()

	if users and arg.signupFilter ~= nil then
		users = utils.removeIf(utils.arrayCopy(users), function (userID)
			return racingUsers.isParticipant(userID) ~= arg.signupFilter
		end)
	end

	if users and not userMenuIncludeOffline then
		users = utils.removeIf(utils.arrayCopy(users), function (userID)
			return not racingUsers.getPlayerID(userID)
		end)
	end

	if not users then
		table.insert(entries, {label = "Loading...", selected = true, hideCursor = true})
		users = {}
	elseif #users == 0 then
		table.insert(entries, {label = "No users"})
	else
		table.insert(entries, {label = "Users"})

		if arg.subtitle then
			table.insert(entries, {
				label = arg.subtitle,
				font = smallLowercaseFont,
			})
		end

		table.insert(entries, {height = 0})
		for _, userID in ipairs(users) do
			local act = arg.action(userID)
			table.insert(entries, {
				id = "user_" .. tostring(userID),
				label = (racingUsers.getName(userID) or "Loading...") .. " " .. playerList.getAttribute(racingUsers.getPlayerID(userID), racingProtocol.PlayerAttribute.IDENTITY_PRONOUNS) .. getStatusSuffix(userID, not arg.nonConnectedSuffix),
				font = mediumLowercaseFont,
				action = act and function ()
					lastSelectedUser = userID
					return act()
				end or nil,
				specialAction = act and function ()
					return act(true)
				end or nil,
				enableIf = act ~= nil,
				selected = (lastSelectedUser == userID)
			})
		end
	end

	table.insert(entries, {height = 0})

	table.insert(entries, {
		label = function ()
			return "Show offline players: " .. textFormat.checkbox(userMenuIncludeOffline, 1)
		end,
		id = "includeOffline",
		font = smallLowercaseFont,
		action = function ()
			userMenuIncludeOffline = not userMenuIncludeOffline
			menu.update()
			menu.scrollToTarget()
		end,
	})

	local function toggleParticipantFilter(left)
		if left then
			if arg.signupFilter == true then
				arg.signupFilter = nil
			else
				arg.signupFilter = arg.signupFilter ~= nil
			end
		else
			if arg.signupFilter == false then
				arg.signupFilter = nil
			else
				arg.signupFilter = not arg.signupFilter
			end
		end
		menu.update()
		menu.scrollToTarget()
	end

	table.insert(entries, {
		label = function ()
			return "Filter: " .. (arg.signupFilter and "Signed-up users only"
				or (arg.signupFilter == false and "Non-signed up users only" or "All users"))
		end,
		id = "signupFilter",
		font = smallLowercaseFont,
		action = toggleParticipantFilter,
		leftAction = function ()
			toggleParticipantFilter(true)
		end,
		rightAction = toggleParticipantFilter,
	})

	table.insert(entries, {height = 0})

	table.insert(entries, {
		id = "done",
		label = arg.doneText or "Done",
		action = function ()
			menu.close()
			menu.update()
		end,
	})

	ev.menu = {
		label = arg.title or "Racing user list",
		entries = entries,
		escapeAction = function ()
			menu.close()
			menu.update()
		end,
	}
end)

event.menu.add("synchronyRacingUserAdmin", "synchronyRacingUserAdmin", function (ev)
	ev.arg = ev.arg or {}

	local entries = {}

	local userID = tostring(ev.arg.user)
	local userName = racingUsers.getName(userID)

	table.insert(entries, {label = userName})
	table.insert(entries, {label = "ID: " .. (userID):sub(1, 15) .. "...", font = smallLowercaseFont})
	table.insert(entries, {label = "Status: " .. getStatus(userID), font = smallLowercaseFont})

	table.insert(entries, {height = 0})

	local function toggleParticipant()
		return racingUsers.setParticipant(userID, not racingUsers.isParticipant(userID))
	end

	table.insert(entries, {
		label = function ()
			return "Signed up " .. textFormat.checkbox(racingUsers.isParticipant(userID))
		end,
		id = "signup",
		action = toggleParticipant,
	})

	local function getRoomID()
		return serverPlayerList.getRoomID(racingUsers.getPlayerID(userID))
	end

	local function getRoomName(roomID)
		roomID = roomID or getRoomID()
		local match = racingRooms.getMatchForRoomID(roomID)
		local teamID = racingRooms.getTeamIndexForRoomID(roomID)
		local team = match and match.teams[teamID]
		return (team and racingAdmin.Match.getTeamLabel(team))
			or (match and racingAdmin.Match.getMatchLabel(match))
			or (roomID == serverRooms.getDefaultRoomID() and "Lobby")
			or roomID
			or "None"
	end

	-- Pre-cache rooms
	serverRooms.listRooms()

	local function changePlayerRoom(diff)
		local rooms = serverRooms.listRooms()
		if rooms and rooms[1] then
			local roomIndex = utils.arrayFind(rooms, ev.arg.targetRoom or getRoomID()) or 0
			ev.arg.targetRoom = rooms[(roomIndex + diff - 1) % #rooms + 1]
		end
	end

	table.insert(entries, {
		label = function ()
			local roomID = getRoomID()
			local str = string.format("Room: %s", getRoomName(roomID))
			if ev.arg.targetRoom and ev.arg.targetRoom ~= roomID then
				str = str .. textFormat.color(" -> " .. getRoomName(ev.arg.targetRoom), theme.Color.HIGHLIGHT)
			end

			-- Precompute room validity for cache
			serverRooms.isValidRoom(roomID)
			serverRooms.isValidRoom(ev.arg.targetRoom)

			return str
		end,
		id = "roomID",
		action = function ()
			-- Move user to target room
			local playerID = racingUsers.getPlayerID(userID)
			if playerID and serverRooms.isValidRoom(ev.arg.targetRoom) then
				serverRooms.setPlayerRoom(playerID, ev.arg.targetRoom)
				ev.arg.targetRoom = nil
			end
		end,
		specialAction = function ()
			-- Move self to target room
			local roomID = ev.arg.targetRoom or getRoomID()
			if serverRooms.isValidRoom(roomID) then
				serverRooms.setPlayerRoom(playerList.getLocalPlayerID(), roomID)
			end
		end,
		rightAction = function ()
			changePlayerRoom(1)
		end,
		leftAction = function ()
			changePlayerRoom(-1)
		end,
	})

	table.insert(entries, {
		label = function ()
			return "Spectator slot: " .. tostring(ev.arg.slot or racingUsers.getSpectatorSlot(userID) or "Off")
		end,
		id = "specSlot",
		action = function ()
		end,
		rightAction = function ()
			ev.arg.slot = ev.arg.slot or racingUsers.getSpectatorSlot(userID)
			ev.arg.slot = ev.arg.slot and ev.arg.slot + 1 or 1
			racingUsers.setSpectatorSlot(userID, ev.arg.slot)
		end,
		leftAction = function ()
			ev.arg.slot = ev.arg.slot or racingUsers.getSpectatorSlot(userID)
			ev.arg.slot = ev.arg.slot and ev.arg.slot > 1 and (ev.arg.slot - 1) or nil
			racingUsers.setSpectatorSlot(userID, ev.arg.slot)
		end,
	})

	table.insert(entries, {
		label = function ()
			return "Admin " .. textFormat.checkbox(racingUsers.isAdmin(userID))
		end,
		id = "admin",
		action = function ()
			if racingUsers.isAdmin(userID) then
				racingUsers.setAdmin(userID, false)
			else
				menu.open("confirm", {
					message = string.format("Are you sure you want to grant admin\npermissions to '%s'?", userName),
					yes = function ()
						racingUsers.setAdmin(userID, true)
					end,
					defaultNo = true,
				})
			end
		end,
	})

	table.insert(entries, {height = 0})

	table.insert(entries, {
		id = "done",
		label = "Done",
		action = menu.close,
	})

	ev.menu = {
		label = "User settings",
		entries = entries,
	}
end)

event.menu.add("synchronyRacingCommentatorMode", "synchronyRacingCommentatorMode", function (ev)
	ev.arg = ev.arg or {}

	ev.arg.userID = ev.arg.userID or racingUsers.lookUpUserByPlayerID(playerList.getLocalPlayerID())

	local specSlot = ev.arg.userID and racingUsers.getSpectatorSlot(ev.arg.userID)

	local entries = {}
	local users = racingUsers.listAllUsers()
	local playerIDs = {}

	if users then
		for i = 1, #users do
			playerIDs[users[i]] = racingUsers.getPlayerID(users[i])
		end
		users = utils.removeIf(utils.arrayCopy(users), function (userID)
			return playerIDs[userID] == playerList.getLocalPlayerID() or not playerIDs[userID]
		end)
	end

	local matches = racingRooms.listMatches()
	local latestMatch = matches and matches[#matches]

	local function setSpec(userID, enabled)
		racingUsers.setSpectatorSlot(userID, enabled and 1 or nil)
		local playerID = playerIDs[userID]
		if playerID and latestMatch then
			if enabled then
				racingSpectator.handleAutoSpectate({id = latestMatch}, { playerID })
			elseif racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.SPECTATOR)
				and not racingProtocol.clientGetPlayerAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT)
			then
				serverRooms.sendPlayerToDefaultRoom(playerID)
			end
		end
	end

	table.insert(entries, {
		label = string.format("%s %s", textFormat.checkbox(specSlot),
			ev.arg.userID and racingUsers.getName(ev.arg.userID) or playerList.getName()),
		id = "commentatorMode",
		action = function ()
			if ev.arg.userID then
				setSpec(ev.arg.userID, not specSlot)
				menu.update()
			end
		end,
		font = mediumLowercaseFont,
		alignX = 0,
		x = -200,
	})

	table.insert(entries, {height = 0})

	if not users then
		table.insert(entries, {label = "Loading...", selected = true, hideCursor = true})
		users = {}
	end

	for _, userID in ipairs(users) do
		local state = racingUsers.getSpectatorSlot(userID)
		table.insert(entries, {
			id = "user_" .. tostring(userID),
			label = string.format("%s %s", textFormat.checkbox(state), racingUsers.getName(userID) or "Loading..."),
			font = mediumLowercaseFont,
			action = function ()
				setSpec(userID, not state)
			end,
			alignX = 0,
			x = -200,
		})
	end

	table.insert(entries, {height = 0})

	table.insert(entries, {
		label = "Open additional instance",
		id = "openInstance",
		action = function ()
			multiInstance.create { external = true, windowTitle = "NDBroadcaster" }
		end,
		enableIf = false,
		hideIfDisabled = true,
	})

	table.insert(entries, {
		id = "done",
		label = "Done",
		action = menu.close,
	})

	ev.menu = {
		label = "Commentator mode",
		entries = entries,
		closeOnRunReset = false,
	}
end)
