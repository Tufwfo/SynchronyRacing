local racingIdentity = {}

local racingHologram = require "SynchronyRacing.RacingHologram"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local gameClient = require "necro.client.GameClient"

local playerList = require "necro.client.PlayerList"
local settings = require "necro.config.Settings"
local tick = require "necro.cycles.Tick"

local enum = require "system.utils.Enum"
local utils = require "system.utils.Utilities"

needIdentityUpdate = true

settings.group {
	id = "identity",
	name = "Player profile",
	autoRegister = true,
}

preferredPronouns = settings.user.string {
	id = "identity.pronouns",
	name = "Pronouns",
	desc = "This choice will be visible to live broadcasters and viewers, allowing them to use appropriate pronouns when referring to you during commentary.",
	default = "",
	maxLength = 15,
	setter = function (value)
		playerList.setAttribute(racingProtocol.PlayerAttribute.IDENTITY_PRONOUNS, value ~= "" and value or nil)
	end,
}

preferredName = settings.user.string {
	id = "identity.username",
	name = "Display name",
	desc = "Specify an alternate display name for yourself. This choice will be visible to live broadcasters and viewers.",
	default = "",
	maxLength = 32,
	setter = function (value)
		playerList.setAttribute(racingProtocol.PlayerAttribute.IDENTITY_NAME, value ~= "" and value or nil)
	end,
	format = function (value)
		if not value or value == "" then
			return tostring(playerList.getName() or gameClient.getUsername() or "")
		else
			return tostring(value)
		end
	end,
}

local function getPronounAsString(playerID)
	local pronouns = playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.IDENTITY_PRONOUNS)
	return utf8.sub(pronouns, 1, 32)
end

local function getEffectiveName(playerID)
	local nameOverride = playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.IDENTITY_NAME)
	if type(nameOverride) == "string" then
		return utf8.sub(nameOverride, 1, 32)
	else
		return playerList.getName(playerID)
	end
end

local hasDebugDump, debugDump = pcall(require, "system.debug.DebugDump")

local function writeRacerIdentity(playerIDs, infix, mapping, index)
	pcall(function ()
		local lines = {}
		for _, playerID in ipairs(playerIDs) do
			lines[#lines + 1] = tostring(mapping(playerID) or ""):gsub("\n", " ")
		end

		debugDump.writeTextFile(string.format("racer%s%s", infix, index), table.concat(lines, "\n"))
	end)
end

local function isNotRacer(playerID)
	return not playerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARTICIPANT)
end

local function updateIdentityFilesImmediately()
	if not hasDebugDump then
		return
	end

	local index = racingHologram.getInstanceIndex()
	if index then
		local playerIDs = utils.removeIf(utils.arrayCopy(playerList.getPlayerList()), isNotRacer)
		writeRacerIdentity(playerIDs, "Name", getEffectiveName, index)
		writeRacerIdentity(playerIDs, "Pronoun", getPronounAsString, index)
		needIdentityUpdate = false
	else
		return false
	end
end

updateIdentityFiles = tick.delay(updateIdentityFilesImmediately)

local function updateOwnPronouns()
	if playerList.getAttribute(nil, racingProtocol.PlayerAttribute.IDENTITY_PRONOUNS) ~= preferredPronouns then
		playerList.setAttribute(racingProtocol.PlayerAttribute.IDENTITY_PRONOUNS, preferredPronouns)
	end
	local name = preferredName ~= "" and preferredName or nil
	if playerList.getAttribute(nil, racingProtocol.PlayerAttribute.IDENTITY_NAME) ~= name then
		playerList.setAttribute(racingProtocol.PlayerAttribute.IDENTITY_NAME, name)
	end
end

event.gameStateEnterLobby.add("updatePronouns", "menu", function (ev)
	updateOwnPronouns()
	updateIdentityFiles(nil, 0.25)
end)

event.gameStateLeaveLobby.add("updatePronouns", "playerAttributes", function (ev)
	updateOwnPronouns()
	updateIdentityFiles(nil, 0.25)
end)

event.gameStateLevel.add("writePronounFile", "replaySave", function (ev)
	if needIdentityUpdate then
		updateIdentityFiles(nil, 0.25)
	end
end)

event.clientPlayerList.add("updatePronouns", racingProtocol.PlayerAttribute.IDENTITY_PRONOUNS, function (ev)
	updateIdentityFiles(nil, 0.25)
end)

return racingIdentity
