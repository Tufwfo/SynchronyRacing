local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"

event.getIntroText.add("racingMode", {order = "modsEnabled", sequence = 1}, function (ev)
	if racingProtocol.clientGetRoomAttribute(racingProtocol.RoomAttribute.ROOM_TYPE) == racingRooms.RoomType.RACE then
		ev.text = "Racing mode!\nComplete all zones as quickly\nas possible!"
	end
end)
