local racingGhost = {}

local racingHologram = require "SynchronyRacing.RacingHologram"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingSpectator = require "SynchronyRacing.RacingSpectator"
local action = require "necro.game.system.Action"
local components = require "necro.game.data.Components"
local currentLevel = require "necro.game.level.CurrentLevel"
local customActions = require "necro.game.data.CustomActions"
local customEntities = require "necro.game.data.CustomEntities"
local facing = require "necro.game.character.Facing"
local gameClient = require "necro.client.GameClient"
local instantReplay = require "necro.client.replay.InstantReplay"
local inventory = require "necro.game.item.Inventory"
local marker = require "necro.game.tile.Marker"
local move = require "necro.game.system.Move"
local netplay = require "necro.network.Netplay"
local object = require "necro.game.object.Object"
local objectRenderer = require "necro.render.level.ObjectRenderer"
local player = require "necro.game.character.Player"
local segmentVisibility = require "necro.game.vision.SegmentVisibility"
local serverActionBuffer = require "necro.server.ServerActionBuffer"
local serverPlayerList = require "necro.server.ServerPlayerList"
local serverRooms = require "necro.server.ServerRooms"
local serverSocket = require "necro.server.ServerSocket"
local settings = require "necro.config.Settings"
local settingsStorage = require "necro.config.SettingsStorage"
local snapshot = require "necro.game.system.Snapshot"
local spectator = require "necro.game.character.Spectator"
local targetPointerHUD = require "necro.render.hud.TargetPointerHUD"
local tick = require "necro.cycles.Tick"
local tile = require "necro.game.tile.Tile"
local visualExtent = require "necro.render.level.VisualExtent"
local ecs = require "system.game.Entities"
local timer = require "system.utils.Timer"
local utils = require "system.utils.Utilities"

local field = components.field
local constant = components.constant
local dependency = components.dependency

local abs = math.abs
local bitOr = bit.bor
local bitAnd = bit.band
local bitLShift = bit.lshift
local bitRShift = bit.rshift

showGhost = settings.overridable.bool {
	name = "Show opponent ghost",
	default = true,
	order = 50,
}

showGhostCompass = settings.overridable.bool {
	name = "Show ghost compass",
	default = true,
	order = 51,
}

lastSentCoords = nil
lastSentItems = nil
lastSentHealth = nil

lastFullSync = 0

ghosts = snapshot.runVariable({})
levelNumber = snapshot.levelVariable(1)

local function reduceCoords(x, y, level, tween)
	return bitOr(bitAnd(0xFF, x + 128),
		bitLShift(bitAnd(0xFF, y + 128), 8),
		bitLShift(bitAnd(0xFF, level), 16),
		bitLShift(bitAnd(0x7F, tween), 24))
end

local function expandCoords(coords)
	return bitAnd(coords, 0xFF) - 128,
		bitAnd(bitRShift(coords, 8), 0xFF) - 128,
		bitAnd(bitRShift(coords, 16), 0xFF),
		bitAnd(bitRShift(coords, 24), 0x7F)
end

local function reduceHealth(hearts, containers, cursed)
	return bitOr(bitAnd(0xFF, hearts),
		bitLShift(bitAnd(0xFF, containers), 8),
		bitLShift(bitAnd(0xFF, cursed), 16))
end

local function expandHealth(health)
	return bitAnd(health, 0xFF),
		bitAnd(bitRShift(health, 8), 0xFF),
		bitAnd(bitRShift(health, 16), 0xFF)
end

event.updateVisuals.add("syncGhostPosition", "coopLink", function (ev)
	if currentLevel.isLobby() or spectator.isSpectating() or instantReplay.isActive()
		or settingsStorage.get("mod.SynchronyRacing.racingGhost.showGhost", settings.Layer.REMOTE_OVERRIDE) == false
	then
		return
	end

	local entity = player.getPlayerEntity()
	if entity and entity.position and entity.eventCache and entity.equipment then
		if timer.getGlobalTime() - lastFullSync > 10 then
			lastFullSync = timer.getGlobalTime()
			lastSentHealth = nil
			lastSentItems = nil
			lastSentCoords = nil
		end

		local health = reduceHealth(entity.health and entity.health.health or 0,
			entity.health and entity.health.maxHealth or 0,
			entity.cursedHealth and entity.cursedHealth.health or 0)
		if lastSentHealth ~= health then
			gameClient.sendReliable(racingProtocol.MessageType.GHOST, {"h", health})
			lastSentHealth = health
		end

		local inventoryKey = entity.eventCache.key
		if lastSentItems ~= inventoryKey then
			local items = {}
			for _, itemID in ipairs(entity.equipment.items) do
				local item = ecs.getEntityByID(itemID)
				if item and not item.soulLink then
					items[#items + 1] = item.name
				end
			end
			gameClient.sendReliable(racingProtocol.MessageType.GHOST, {"i", items})
			lastSentItems = inventoryKey
		end

		local coords = reduceCoords(entity.position.x, entity.position.y, levelNumber,
			entity.tween and entity.tween.turnID >= 0 and 1 or 0)
		if lastSentCoords ~= coords then
			gameClient.sendUnreliable(racingProtocol.MessageType.GHOST, coords)
			lastSentCoords = coords
		end
	end
end)

components.register {
	SynchronyRacing_playerGhost = {
		field.int("playerID"),
		field.int("level"),
		field.int("x"),
		field.int("y"),
	},
}

customEntities.extend {
	name = "SynchronyRacing_PlayerGhost",
	template = customEntities.template.player(),
	components = {
		{
			friendlyName = { name = "Player ghost" },

			opacityEffect = { baseOpacity = 0.4 },
			sprite = {
				texture = "ext/entities/player1_armor_body.png",
				width = 24,
				height = 24,
			},
			rowOrder = { z = -10 },
			visibilityIgnoreSegments = {},
			minimapBlinkingPixel = { texture = "mods/SynchronyRacing/gfx/minimap_player_pixel.png" },

			collision = false,
			playableCharacter = false,
			trappable = false,
			visionRaycast = false,
			attackable = false,
			targetable = false,
			lightSource = false,
			interactor = false,
			proximityVision = false,
			proximityReveal = false,
			confusable = false,
			knockbackable = false,
			stasisApproacher = false,
			bossFighter = false,
			teleportOnBossFightStart = false,
			spectator = false,
			spectatorIntangible = false,
			descentActivateOnTile = false,
			descentIntangibleOnCompletion = false,
			descentPersistToNextLevel = false,
			descentExitLevel = false,
			provocationProximityTarget = false,
			sacrificable = false,
			tileIdleDamageReceiver = false,

			health = {},
			cursedHealth = {},
			SynchronyRacing_playerGhost = {},

			initialInventory = { items = {} },
			initialInventoryUnlockReceiver = false,
			initialInventoryTrainingWeaponReceiver = false,
			itemUser = false,
			itemSlotUser = {
				slots = { hud = true, body = true, shield = true }
			},
			itemCollector = false,

			DynChar_dynamicCharacter = {},
			DynChar_dynamicCharacterShowUnequippedItems = {},
			CharacterSkins_skinCharacterName = { name = "Cadence" },
			editorHidden = {},
		},
		{
			sprite = {
				texture = "ext/entities/player1_heads.png",
				width = 24,
				height = 24,
			},
			CharacterSkins_skinCharacterName = { name = "Cadence" },
		},
	},
}

local function resolveGhostPosition(entity, moveFlag)
	local x, y = entity.SynchronyRacing_playerGhost.x, entity.SynchronyRacing_playerGhost.y
	local remoteLevel, ownLevel = entity.SynchronyRacing_playerGhost.level, levelNumber
	if remoteLevel > ownLevel then
		x, y = marker.lookUpFirst(marker.Type.STAIRS)
		if x then
			return x, y, 0
		else
			return 0, 0, 0
		end
	elseif remoteLevel < ownLevel then
		return 0, 0, 0
	else
		return x, y, moveFlag
	end
end

local ghostAction = customActions.registerSystemAction {
	id = "G",
	callback = function (playerID, args)
		local ghost = ecs.getEntityByID(ghosts[playerID])
		local new = false
		if not ghost then
			new = true
			ghost = object.spawn("SynchronyRacing_PlayerGhost", 0, 0, {
				SynchronyRacing_playerGhost = { playerID = playerID },
				CharacterSkins_skinnable = {
					playerID = playerID,
				},
				gameObject = { persist = true },
			})
			ghosts[playerID] = ghost.id
		end
		if type(args) == "number" then
			local x, y, level, tween = expandCoords(args)
			-- Don't tween ghosts across long distances
			local dx, dy = x - ghost.SynchronyRacing_playerGhost.x, y - ghost.SynchronyRacing_playerGhost.y
			if ghost.SynchronyRacing_playerGhost.level ~= level or abs(dx) + abs(dy) > 4 then
				tween = 0
			end

			local direction = action.move(dx, dy)
			if direction ~= 0 then
				facing.setDirection(ghost, direction)
			end

			ghost.SynchronyRacing_playerGhost.x = x
			ghost.SynchronyRacing_playerGhost.y = y
			ghost.SynchronyRacing_playerGhost.level = level
			move.absolute(ghost, resolveGhostPosition(ghost, not new and tween == 1 and move.Flag.TWEEN or 0))
			ghost.rowOrder.z = tile.getInfo(x, y).isWall and 10 or -10
		elseif type(args) == "table" then
			if args[1] == "i" and type(args[2]) == "table" then
				local oldItems = inventory.getItems(ghost)
				local foundOldItemSet = {}
				local grantList = {}
				for _, itemName in ipairs(args[2]) do
					local found = false
					for i, oldItem in ipairs(oldItems) do
						if oldItem.name == itemName then
							foundOldItemSet[i] = true
							found = true
							break
						end
					end
					if not found then
						grantList[#grantList + 1] = itemName
					end
				end
				-- Delete old items that are no longer present
				for i, item in ipairs(oldItems) do
					if not foundOldItemSet[i] then
						object.delete(item)
					end
				end
				-- Add new items
				for _, itemName in ipairs(grantList) do
					if ecs.typeHasComponent(itemName, "item") and not ecs.typeHasComponent(itemName, "soulLink") then
						inventory.replace(itemName, ghost)
					end
				end
			elseif args[1] == "h" and type(args[2]) == "number" then
				ghost.health.health, ghost.health.maxHealth, ghost.cursedHealth.health = expandHealth(args[2])
			end
		end
	end,
}

local function addRoomAction(playerID, actionID, roomID, args)
	if serverRooms.isValidRoom(roomID) and serverRooms.getGameState(roomID).state == netplay.GameState.IN_GAME then
		-- Pack action ID and args together into table, or send raw actionID in case of no args
		local actionData = args and {actionID, args} or actionID

		-- Add action to buffer
		local turnID = (serverActionBuffer.getLatestTurnID(roomID) or 0) + 1
		local message = serverActionBuffer.getActionBuffer(roomID).add(playerID, turnID, actionData, 0)

		-- Check result to avoid errors when a client attempts to override their own action
		if message then
			message[0] = playerID

			-- Broadcast action to all players
			serverSocket.sendReliable(netplay.MessageType.PLAYER_INPUT, message,
				serverRooms.playersInRoom(roomID), netplay.Channel.GAME)

			-- Return the action's sequence ID
			return message[5]
		end
	end
	return false
end

addRoomActionDeferred = tick.delay(function (args)
	-- Give up after 10s
	if timer.getGlobalTime() - args.time > 10 then
		return
	end

	-- Retry adding action
	return addRoomAction(args.player, args.action, args.room, args.args)
end)

event.levelLoad.add("ghostLevelNumber", {order = "lobbyLevel", sequence = 1}, function (ev)
	levelNumber = ev.SynchronyRacing_levelNumberOverride or ev.number or 1
end)

event.gameStateLevel.add("relocateGhosts", "placePlayers", function (ev)
	for entity in ecs.entitiesWithComponents {"SynchronyRacing_playerGhost"} do
		local x, y = resolveGhostPosition(entity)
		move.absolute(entity, x, y, 0)
	end
end)

event.objectUpdateTangibility.add("ghostIntangible", {order = "follower", filter = "SynchronyRacing_playerGhost"},
function (ev)
	ev.tangible = false
end)

event.serverMessage.add("broadcastGhostPosition", racingProtocol.MessageType.GHOST, function (ev)
	local roomID = serverPlayerList.getRoomID(ev.playerID)
	local match = racingRooms.getMatchForRoomID(roomID)
	local teamIndex = racingRooms.getTeamIndexForRoomID(roomID)
	if match then
		for index, team in ipairs(match.teams) do
			if index ~= teamIndex then
				local success = addRoomAction(ev.playerID, ghostAction, team.room, ev.message)
				if not success then
					addRoomActionDeferred({
						time = timer.getGlobalTime(),
						player = ev.playerID,
						action = ghostAction,
						room = team.room,
						args = ev.message,
					})
				end
			end
		end
	end
end)

local function isDead(entity)
	return (not entity.health or entity.health.health <= 0)
		and (not entity.cursedHealth or entity.cursedHealth.health <= 0)
end

local function setHeadVisibility(entity, visible)
	local head = ecs.getEntityByID(entity.characterWithAttachment and entity.characterWithAttachment.attachmentID) or entity
	if head and head.visibility then
		head.visibility.visible = visible
	end
end

local function isHiddenByHologram()
	return racingHologram.isScreenWatchClient()
		or (racingSpectator.isClientSpectator() and racingHologram.getViewCount() > 0)
end

event.visibility.add("updateGhostVisibility", {order = "minimapVisibility", sequence = 1}, function (ev)
	if showGhost and not isHiddenByHologram() then
		for entity in ecs.entitiesWithComponents {"SynchronyRacing_playerGhost", "visibility", "minimapBlinkingPixel"} do
			local visible = not isDead(entity) and entity.SynchronyRacing_playerGhost.level == levelNumber
			entity.visibility.visible = visible and segmentVisibility.isVisible(entity)
			entity.minimapBlinkingPixel.visible = visible
			setHeadVisibility(entity, entity.visibility.visible)
		end
	else
		for entity in ecs.entitiesWithComponents {"SynchronyRacing_playerGhost", "visibility"} do
			entity.visibility.visible = false
			setHeadVisibility(entity, false)
		end
	end
end)

event.render.add("ghostCompass", "contents", function (ev)
	if not showGhostCompass or isHiddenByHologram() then
		return
	end

	for entity in ecs.entitiesWithComponents {"SynchronyRacing_playerGhost", "sprite"} do
		local dead = isDead(entity)
		local x, y = visualExtent.getPosition(entity)
		x, y = x / 24, y / 24
		local head = ecs.getEntityByID(entity.characterWithAttachment and entity.characterWithAttachment.attachmentID) or entity
		local visual = objectRenderer.getObjectVisual(head)
		local levelDiff = utils.clamp(-1, entity.SynchronyRacing_playerGhost.level - levelNumber, 1)
		targetPointerHUD.draw {
			x = x,
			y = y,
			image = "mods/SynchronyRacing/gfx/player_arrow.png",
			texRect = {0, 19 * (dead and 0 or (levelDiff + 1)), 29, 18},
			rotate = true,
			ignoreOnScreen = true,
			scale = 2,
		}
		if dead then
			targetPointerHUD.draw {
				x = x,
				y = y,
				image = "mods/SynchronyRacing/gfx/player_death.png",
				ignoreOnScreen = true,
				scale = 2,
			}
		else
			targetPointerHUD.draw {
				x = x,
				y = y,
				image = visual.texture,
				ignoreOnScreen = true,
				texRect = visual.texRect,
				scale = 2,
			}
		end
	end
end)



return racingGhost
