local racingUsers = {}

local racingMatch = require "SynchronyRacing.RacingMatch"
local racingProtocol = require "SynchronyRacing.RacingProtocol"
local racingRooms = require "SynchronyRacing.RacingRooms"
local racingSpectator = require "SynchronyRacing.RacingSpectator"

local event = require "necro.event.Event"
local serverChat = require "necro.server.ServerChat"
local serverRooms = require "necro.server.ServerRooms"
local settings = require "necro.config.Settings"
local netplay = require "necro.network.Netplay"

local serverPlayerList = require "necro.server.ServerPlayerList"

local timer = require "system.utils.Timer"
local utils = require "system.utils.Utilities"


--- @class SynchronyRacingUser
--- @field id string
--- @field name string
--- @field available boolean
--- @field lastPlayed number UNIX timestamp of the user's most recent match
--- @field spectatorSlot integer|nil Index of the team this user automatically spectates (nil for no spectating)
--- @field participant boolean|nil Includes/excludes the user from matchmaking and the team selection list
--- @field admin boolean

--- @type table<string,SynchronyRacingUser>
users = settings.user.table {}

autoSetParticipants = settings.shared.bool {
	name = "Auto-register racers",
	default = true,
	order = 60,
}

local userInitPending = true

--- @param userID string ID of the user
--- @return SynchronyRacingUser
local function getUser(userID)
	return users[userID] or {}
end

local function isConnected(userID)
	return serverPlayerList.isConnected(racingUsers.getPlayerID(userID))
end

local function resetUserAvailability()
	for userID, user in pairs(users) do
		user.available = racingUsers.isAvailable(userID)
	end
end

function racingUsers.lookUpUserByPlayerID(playerID)
	return serverPlayerList.getAttribute(playerID, netplay.PlayerAttribute.REJOIN_TOKEN)
end

function racingUsers.getName(userID)
	return getUser(userID).name
end

function racingUsers.getPlayerID(userID)
	return serverPlayerList.lookUpPlayerByRejoinToken(userID)
end

function racingUsers.isValid(userID)
	return getUser(userID) ~= nil
end

function racingUsers.setGuest(userID, guest)
	local playerID = racingUsers.getPlayerID(userID)
	if serverPlayerList.isConnected(playerID) then
		serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.GUEST, guest and true or nil)
	end
end

function racingUsers.setSpectatorSlot(userID, spectatorSlot)
	getUser(userID).spectatorSlot = spectatorSlot

	local playerID = racingUsers.getPlayerID(userID)
	if playerID then
		if spectatorSlot then
			serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.GUEST, nil)
		end
		serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.BROADCASTER, spectatorSlot)
	end
end

function racingUsers.getSpectatorSlot(userID)
	return getUser(userID).spectatorSlot
end

function racingUsers.updateLastPlayed(userID)
	getUser(userID).lastPlayed = timer.unixTimestamp()
end

function racingUsers.isInMatch(userID)
	local match = racingRooms.getMatchForRoomID(serverPlayerList.getRoomID(racingUsers.getPlayerID(userID)))
	return match ~= nil
end

function racingUsers.setParticipant(userID, participant)
	getUser(userID).participant = participant
	if participant then
		racingUsers.setGuest(userID, false)
	end
end

function racingUsers.isParticipant(userID)
	local participant = getUser(userID).participant
	if participant ~= nil then
		return participant
	elseif autoSetParticipants then
		return racingUsers.getSpectatorSlot(userID) == nil
	else
		return false
	end
end

function racingUsers.setAdmin(userID, admin)
	getUser(userID).admin = admin

	local playerID = racingUsers.getPlayerID(userID)
	if playerID then
		serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.ADMIN, admin)
	end
end

function racingUsers.isAdmin(userID)
	return getUser(userID).admin
end

function racingUsers.setAvailable(userID, available)
	getUser(userID).available = available
end

function racingUsers.isAvailable(userID)
	return getUser(userID).available and isConnected(userID) and not racingUsers.isInMatch(userID)
		and racingUsers.isParticipant(userID)
end

function racingUsers.getAvailableUsers()
	return utils.map(utils.sort(utils.removeIf(utils.getValueList(users), function (user)
		return not racingUsers.isAvailable(user.id)
	end), function (user1, user2)
		return (tonumber(user1.lastPlayed) or math.huge) < (tonumber(user2.lastPlayed) or math.huge)
	end), function (user)
		return user.id
	end)
end

function racingUsers.listAllUsers()
	return utils.map(utils.sort(utils.getValueList(users), function (user1, user2)
		return tostring(user1.name):lower() < tostring(user2.name):lower()
	end), function (user) return user.id end)
end

function racingUsers.addUser(userID, name)
	if type(userID) == "string" and not users[userID] then
		users[userID] = {
			id = userID,
			name = name,
			lastPlayed = timer.unixTimestamp(),
			guest = not autoSetParticipants,
		}
		return userID
	end
end

function racingUsers.addConnectedPlayer(playerID)
	if not serverPlayerList.getAttribute(playerID, racingProtocol.PlayerAttribute.PARENT) then
		local userID = racingUsers.lookUpUserByPlayerID(playerID)
		racingUsers.addUser(userID, serverPlayerList.getName(playerID))
		serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.ADMIN, racingUsers.isAdmin(userID))
		serverPlayerList.setAttribute(playerID, racingProtocol.PlayerAttribute.BROADCASTER, racingUsers.getSpectatorSlot(userID))
		return userID
	end
end

event.serverTick.add("processRacingUsers", {order = "conductor", sequence = -1}, function (ev)
	if userInitPending then
		userInitPending = false
		for _, playerID in ipairs(serverPlayerList.allPlayers()) do
			racingUsers.addConnectedPlayer(playerID)
		end
		resetUserAvailability()
	end
end)

event.serverMessage.add("receivePlayerAttribute", {key = "PLAYER_ATTRIBUTE", sequence = 1}, function (ev)
	local key, value = ev.message[1], ev.message[2]
	if serverPlayerList.isLoggedIn(ev.playerID) and serverPlayerList.isAttributeUserSettable(key) then
		if key == netplay.PlayerAttribute.CHARACTER then
			racingUsers.setAvailable(racingUsers.lookUpUserByPlayerID(ev.playerID), not not value)
		end

		if key == netplay.PlayerAttribute.SPECTATING then
			local match = racingRooms.getMatchForRoomID(serverPlayerList.getRoomID(ev.playerID))
			if match then
				local spectating = serverPlayerList.getAttribute(ev.playerID, racingProtocol.PlayerAttribute.SPECTATOR)
				if spectating ~= nil then
					serverPlayerList.setAttribute(ev.playerID, key, spectating)
				end
			end
		end

		if key == racingProtocol.PlayerAttribute.IDENTITY_NAME
			and not serverPlayerList.getAttribute(ev.playerID, racingProtocol.PlayerAttribute.PARENT)
		then
			local oldName = serverPlayerList.getName(ev.playerID)
			if not serverPlayerList.getAttribute(ev.playerID, racingProtocol.PlayerAttribute.SYSTEM_NAME) then
				serverPlayerList.setAttribute(ev.playerID, racingProtocol.PlayerAttribute.SYSTEM_NAME, oldName)
			end
			local newName = serverPlayerList.getAttribute(ev.playerID, racingProtocol.PlayerAttribute.SYSTEM_NAME)
			if type(value) == "string" and value ~= "" then
				newName = utf8.sub(value, 1, 32)
			end
			if newName ~= oldName then
				serverPlayerList.setAttribute(ev.playerID, netplay.PlayerAttribute.NAME, newName)
				local roomID = serverPlayerList.getRoomID(ev.playerID)
				serverChat.sendSystemMessage(string.format("%s changed name to %s", oldName, newName),
					serverRooms.isValidRoom(roomID) and serverRooms.playersInRoom(roomID) or {})
			end
		end
	end
end)

local function isGuest(userID)
	if userID and not autoSetParticipants then
		local user = getUser(userID)
		if not user.participant and not user.admin and not user.spectatorSlot then
			return true
		end
	end
end

event.serverLogIn.add("signUpForRacing", {order = "room", sequence = -1}, function (ev)
	if type(ev.message) == "table" and serverPlayerList.isConnected(ev.message.parent) then
		serverPlayerList.setAttribute(ev.playerID, racingProtocol.PlayerAttribute.PARENT, ev.message.parent)
		local roomID = serverPlayerList.getRoomID(ev.message.parent)
		local targetRoomID = racingSpectator.initAutoSpectateOpponent(racingRooms.getMatchForRoomID(roomID), ev.playerID,
			racingProtocol.serverGetPlayerAttribute(ev.message.parent, racingProtocol.PlayerAttribute.BROADCASTER), false)
		if targetRoomID then
			ev.roomID = targetRoomID
			-- TODO promote to host (deferred)
		end
	else
		local userID = racingUsers.addConnectedPlayer(ev.playerID)
		racingProtocol.serverSetPlayerAttribute(ev.playerID, racingProtocol.PlayerAttribute.GUEST, isGuest(userID))
	end
end)

event.serverReset.add("resetUserAvailability", "playerList", function (ev)
	resetUserAvailability()
end)

return racingUsers
