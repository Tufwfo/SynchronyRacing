local event = require "necro.event.Event"
local itemBan = require "necro.game.item.ItemBan"
local menu = require "necro.menu.Menu"
local remoteSettings = require "necro.config.RemoteSettings"
local settings = require "necro.config.Settings"
local ui = require "necro.render.UI"

local color = require "system.utils.Color"
local ecs = require "system.game.Entities"
local gfx = require "system.gfx.GFX"
local utils = require "system.utils.Utilities"

local settingName = "mod.SynchronyRacing.bannedItems"

local function getBannedItemCount(value)
	local count = 0
	for itemName in pairs(remoteSettings.getLocalOverrideValue(settingName) or value or {}) do
		if ecs.typeHasComponent(itemName, "item") and not ecs.typeHasComponent(itemName, "itemPoolUncertainty") then
			count = count + 1
		end
	end
	return count
end

bannedItems = settings.entitySchema.table {
	id = "bannedItems",
	format = function (value)
		local count = getBannedItemCount(value)
		return count ~= 0 and string.format("%s items", count) or "None"
	end,
}

editBannedItems = settings.shared.action {
	name = function ()
		local count = getBannedItemCount(bannedItems)
		return string.format("Banned items: %s", count ~= 0 and count or "None")
	end,
	order = 2,
	action = function ()
		menu.open("SynchronyRacing_ItemBans")
	end,
}

local function isBanned(typeName)
	local setting = remoteSettings.getLocalOverrideValue(settingName) or bannedItems
	return not setting or setting[typeName]
end

local function getIcon(entity, i, bannedFunc)
	local sprite = entity.sprite
	local icon = {
		image = sprite.texture,
		imageRect = {sprite.textureShiftX, sprite.textureShiftY, sprite.width, sprite.height},
		width = sprite.width,
		height = sprite.height,
	}
	return function ()
		local selected, banned = menu.getSelectedID() == i, bannedFunc()
		icon.color = color.hsv(0, 0, math.max((selected and 1 or 0), (banned and 0.8 or 0.3)))
		return icon
	end
end

local function getEntry(entity, x, y, i, bannedFunc)
	return {
		x = x,
		y = y,
		icon = getIcon(entity, i, bannedFunc),
		selectableIf = function () end,
		action = function () end,
	}
end

local columns = 19

local nameOverrides = {
	EnemyPlaceholder = "Enemies in crates",
}

event.menu.add("itemBans", "SynchronyRacing_ItemBans", function (ev)
	local entities = {}

	for _, entity in ecs.prototypesWithComponents {"item", "!itemCurrency", "!itemPoolUncertainty", "!itemNoReturn"} do
		entities[#entities + 1] = entity
	end

	local uncertainItems = {}
	for _, entity in ecs.prototypesWithComponents {"itemPoolUncertainty"} do
		local baseEntityName = entity.name:gsub("Uncertain", "")
		if baseEntityName ~= entity.name and ecs.isValidEntityType(baseEntityName) then
			uncertainItems[baseEntityName] = entity.name
		end
	end

	local function setBanned(typeName, banned)
		if remoteSettings.hasPermissions() then
			local setting = utils.fastCopy(remoteSettings.getLocalOverrideValue(settingName) or bannedItems) or {}
			setting[typeName] = banned or nil
			remoteSettings.setLocalOverrideValue(settingName, setting)

			if uncertainItems[typeName] then
				return setBanned(uncertainItems[typeName], banned)
			end
		end
	end

	local function setAllBanned(banned)
		for _, entity in ecs.prototypesWithComponents {"item"} do
			setBanned(ecs.getEntityTypeName(entity), banned)
		end
	end

	table.sort(entities, function (e1, e2)
		local slot1, slot2 = e1.itemSlot and e1.itemSlot.name or "", e2.itemSlot and e2.itemSlot.name or ""
		if slot1 ~= slot2 then
			return slot1 < slot2
		else
			local weapon1, weapon2 = e1.weaponType and e1.weaponType.name or "", e2.weaponType and e2.weaponType.name or ""
			if weapon1 ~= weapon2 then
				return weapon1 < weapon2
			else
				return e1.name < e2.name
			end
		end
	end)

	local entries = {}

	local bannedText = ui.colorizeText("X", color.rgb(200, 0, 0))
	local bannedSuffix = ui.colorizeText(" (banned)", color.RED)
	local font = utils.mergeDefaults(ui.Font.MEDIUM, { uppercase = false })

	for i, entity in ipairs(entities) do
		local function menuOffset(diff)
			return function ()
				if i + diff <= #entities and i + diff > 0 then
					menu.selectByID(i + diff)
				elseif diff > 0 then
					menu.selectByID("_enableAll")
				elseif diff < 0 then
					menu.selectByID("_done")
				end
				menu.playSelectionSound(diff)
			end
		end

		local x = ((i - 1) % columns - (columns - 1) / 2) * 48
		local y = math.floor((i - 1) / columns) * 48
		local typeName = entity.name

		if entity:hasComponent("sprite") then
			entries[#entries + 1] = getEntry(entity, x, y, i, function ()
				return isBanned(typeName)
			end)
		end

		entries[#entries + 1] = {
			id = i,
			x = x,
			y = y,
			font = font,
			label = isBanned(typeName) and bannedText or "",
			action = function ()
				setBanned(typeName, not isBanned(typeName))
				menu.update()
			end,
			specialAction = function ()
				setBanned(typeName, not isBanned(typeName))
				menu.changeSelection(1)
				menu.update()
			end,
			upAction = menuOffset(-columns),
			downAction = menuOffset(columns),
			rightAction = menuOffset(1),
			leftAction = menuOffset(-1),
			rightSound = "",
			leftSound = "",
			friendlyName = nameOverrides[typeName] or (entity.friendlyName and entity.friendlyName.name or typeName),
			typeName = typeName,
		}
	end

	entries[#entries + 1] = {height = 0}

	entries[#entries + 1] = {
		id = "_enableAll",
		label = "Reset",
		action = function ()
			setAllBanned(false)
			menu.update()
		end,
	}

	entries[#entries + 1] = {
		id = "_done",
		label = "Done",
		action = function ()
			menu.close()
			menu.update()
		end,
		sound = "UIBack",
	}

	entries[#entries + 1] = {
		y = gfx.getHeight() - 120,
		sticky = true,
		label = function ()
			local entry = menu.getSelectedEntry()
			if entry then
				return isBanned(entry.typeName) and (entry.friendlyName .. bannedSuffix) or entry.friendlyName
			end
		end,
		selectableIf = false,
		color = color.WHITE,
	}

	ev.menu = {
		label = "Banned items",
		entries = entries,
		directionalConfirmation = false,
		escapeAction = function ()
			menu.close()
			menu.update()
		end,
		footerSize = 50,
	}
end)

event.itemBanCheck.add("banByComponent", {order = "component"}, function (ev)
	if bannedItems[ev.item.name] then
		ev.mask = itemBan.Flag.mask(ev.mask, itemBan.Type.FULL)
	end
end)
