local racingUnattended = {}

local racingMatch = require "SynchronyRacing.RacingMatch"
local racingRooms = require "SynchronyRacing.RacingRooms"

local gameServer = require "necro.server.GameServer"
local serverChat = require "necro.server.ServerChat"
local serverClock = require "necro.server.ServerClock"
local serverPlayerList = require "necro.server.ServerPlayerList"

local event = require "necro.event.Event"
local settings = require "necro.config.Settings"

conditionFulfilledTimestamp = nil

unattendedGroup = settings.group {
	id = "unattended",
	name = "Unattended mode",
	order = 2010,
}

unattendedEnabled = settings.shared.bool {
	id = "unattended.enabled",
	name = "Enable unattended matchmaking",
	default = false,
}

unattendedMatchLimit = settings.shared.number {
	id = "unattended.matchLimit",
	name = "Maximum simultaneous matches",
	default = 1,
	step = 1,
	minimum = 1,
	maximum = 100,
}

unattendedInterval = settings.shared.time {
	id = "unattended.interval",
	name = "Lobby creation interval",
	default = 10,
	step = 1,
}

unattendedLobbyTimeout = settings.shared.time {
	id = "unattended.lobbyTimeout",
	name = "Match lobby timeout",
	default = 120,
	step = 30,
}

event.tick.add("processUnattended", {order = "networkServer", sequence = -4}, function (ev)
	if gameServer.isOpen() and unattendedEnabled then
		local matches = racingRooms.listMatches()
		if #matches < unattendedMatchLimit then
			if conditionFulfilledTimestamp == nil then
				conditionFulfilledTimestamp = serverClock.getTime()
			elseif serverClock.getTime() > conditionFulfilledTimestamp + unattendedInterval then
				local match = racingRooms.create()
				if match then
					conditionFulfilledTimestamp = nil
				end
			end
		else
			conditionFulfilledTimestamp = nil
		end

		for _, matchID in ipairs(matches) do
			local match = racingRooms.getMatch(matchID)
			if match.status == racingMatch.Status.WAITING
				and serverClock.getTime() > match.createTimestamp + unattendedLobbyTimeout
			then
				-- Close lobby, sending idle players into spectator mode
				serverChat.sendSystemMessage("Race cancelled due to inactivity")
				racingRooms.close(match, true)
			end
		end
	end
end)

return racingUnattended
